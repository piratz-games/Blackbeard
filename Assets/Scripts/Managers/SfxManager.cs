using System;
using System.Collections.Generic;
using Helpers;
using Sirenix.Serialization;
using UnityEngine;

namespace Managers
{
    public enum SoundEffects
    {
        Hurt,
        Block,
        Parry,
        Heal,
        UI_Unable,
        UI_OK
    }
    
    [Serializable]
    public struct SoundEffect
    {
        public AudioClip sound;
        [Range(0, 1f)]
        public float pitchRandomness;
    }
    
    public class SfxManager : SerializedSingleton<SfxManager>
    {
        [OdinSerialize]
        public Dictionary<SoundEffects, SoundEffect> SfxDatabase;
        public AudioSource globalAudioSource;

        public void PlaySfx(SoundEffects id, AudioSource customSource = null)
        {
            if (!SfxDatabase.ContainsKey(id)) return;
            
            SoundEffect targetSfx = SfxDatabase[id];
            
            if (customSource)
            {
                customSource.pitch = UnityEngine.Random.Range(1 - targetSfx.pitchRandomness, 1 + targetSfx.pitchRandomness);
                
                customSource.PlayOneShot(targetSfx.sound);
            }
            else
            {
                globalAudioSource.PlayOneShot(targetSfx.sound);
            }
        }
    }
}
