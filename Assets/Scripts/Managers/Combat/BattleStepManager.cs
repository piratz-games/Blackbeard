using System.Collections.Generic;
using Abstractions.Combat;
using Helpers;
using UnityEngine.Events;

namespace Managers.Combat
{
    public class BattleStepManager : Singleton<BattleStepManager>
    {
        private InputManager inputManager;
        
        private Stack<BattleAction> actionStack = new Stack<BattleAction>();
        private BattleAction currentStep = null;

        public class BattleActionEvent : UnityEvent { };
        public readonly BattleActionEvent OnNewAction = new BattleActionEvent();
        public readonly BattleActionEvent OnUndoAction = new BattleActionEvent();

        protected void Awake()
        {
            inputManager = new InputManager();
            inputManager.Player.Enable();
            inputManager.Player.Undo.performed += (context => UndoStep());
        }

        private void OnDisable()
        {
            inputManager.Player.Disable();
            inputManager.Player.Undo.performed -= (context => UndoStep());
        }

        public void ClearUndoStack()
        {
            actionStack = new Stack<BattleAction>();
        }
        
        private void CreateNewStep()
        {
            BattleAction step = new BattleAction {Actions = new List<Abstractions.Combat.BattleStep>()};
            currentStep = step;
            OnNewAction?.Invoke();
        }

        public void AddActionToCurrentStep(BattleStep act, bool finishStep = false)
        {
            if(currentStep == null) CreateNewStep();
            currentStep.Actions.Add((act));
            if(finishStep) FinishStep();
        }

        private void FinishStep()
        {
            foreach (BattleStep act in currentStep.Actions)
            {
                act.AdvanceAction?.Invoke();
            }
            
            actionStack.Push(currentStep);
            CreateNewStep();
        }

        private void UndoStep()
        {
            if(!BattleManager.Instance.PlayableTurn()|| actionStack.Count <= 0) return;

            BattleAction lastStep = actionStack.Pop();

            foreach (Abstractions.Combat.BattleStep act in lastStep.Actions)
            {
                act.UndoAction?.Invoke();
            }

            OnUndoAction?.Invoke();
            CreateNewStep();
        }
    }

    public class BattleAction
    {
        public List<BattleStep> Actions = new List<BattleStep>();
    }
}