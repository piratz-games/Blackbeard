using System;
using System.Collections.Generic;
using System.Linq;
using Abstractions;
using Abstractions.Combat;
using DG.Tweening;
using Helpers;
using MonoBehaviours.Combat;
using Sirenix.OdinInspector;
using UnityEngine;
using UnityEngine.Tilemaps;

namespace Managers.Combat
{
    public class GridManager : Singleton<GridManager>
    {
        public Tilemap tilemap;
        private List<BattleTile> battleGrid;

        [BoxGroup("Battle Grid parsing markers")]
        public Sprite pit;

        public float opacity = 0.75f;
        public float snapDelay = 2f;
        public float moveDelay = 0.5f;
        public float enemyActDelay = 0.3f;

        public GameObject hitboxMarkerPrefab;

        public void Awake()
        {
            battleGrid ??= ParseTilemap(tilemap);
        }

        private void Start()
        {
            DOTween.ToAlpha(() => tilemap.color, x => tilemap.color = x, 0f, 0f);
        }

        public void FadeIn(Action callback = null)
        {
            DOTween.ToAlpha(() => tilemap.color, x => tilemap.color = x, opacity, moveDelay).OnComplete(() =>
            {
                callback?.Invoke();
            });
        }

        public void FadeOut(Action callback = null)
        {
            DOTween.ToAlpha(() => tilemap.color, x => tilemap.color = x, 0f, moveDelay).OnComplete(() =>
            {
                callback?.Invoke();
            });
        }
        
        private List<BattleTile> ParseTilemap(Tilemap map)
        {
            Vector3Int size = tilemap.size;
            List<BattleTile> grid = new List<BattleTile>();
            
            foreach (Vector3Int pos in tilemap.cellBounds.allPositionsWithin)
            {
                Sprite sp = tilemap.GetSprite(pos);
                if(sp == null) continue;

                BattleTile thisTile = new BattleTile
                {
                    Coordinates = (Vector2Int) pos,
                    Tile = tilemap.GetTile(pos),
                    IsWalkable = true
                };
                
                if (sp == pit) thisTile.IsWalkable = false;

                grid.Add(thisTile);
            }
            
            return grid;
        }

        public BattleTile GetBattleTile(Vector2Int pos)
        {
            return battleGrid.FirstOrDefault(t => t.Coordinates == pos);
        }
        
        public BattleTile GetRandomBattleTile()
        {
            return battleGrid[UnityEngine.Random.Range(0, battleGrid.Count)];
        }

        public List<BattleTile> GetTileNeighbors(BattleTile tile, int distance = 1, bool walkableOnly = false, bool includeDiagonals = false)
        {
            List<BattleTile> neighbors = new List<BattleTile>();
            
            BattleTile topTile = GetBattleTile(new Vector2Int(tile.Coordinates.x, tile.Coordinates.y + distance));
            if (topTile != null && (!walkableOnly || (topTile.IsWalkable))) neighbors.Add(topTile);

            BattleTile botTile = GetBattleTile(new Vector2Int(tile.Coordinates.x, tile.Coordinates.y - distance));
            if (botTile != null && (!walkableOnly || (botTile.IsWalkable))) neighbors.Add(botTile);

            BattleTile eastTile = GetBattleTile(new Vector2Int(tile.Coordinates.x + distance, tile.Coordinates.y));
            if (eastTile != null && (!walkableOnly || (eastTile.IsWalkable))) neighbors.Add(eastTile);

            BattleTile westTile = GetBattleTile(new Vector2Int(tile.Coordinates.x - distance, tile.Coordinates.y));
            if (westTile != null && (!walkableOnly || (westTile.IsWalkable))) neighbors.Add(westTile);

            if (includeDiagonals)
            {
                BattleTile neTile = GetBattleTile(new Vector2Int(tile.Coordinates.x + distance, tile.Coordinates.y + distance));
                if (neTile != null && (!walkableOnly || (neTile.IsWalkable))) neighbors.Add(neTile);
            
                BattleTile seTile = GetBattleTile(new Vector2Int(tile.Coordinates.x + distance, tile.Coordinates.y - distance));
                if (seTile != null && (!walkableOnly || (seTile.IsWalkable))) neighbors.Add(seTile);
            
                BattleTile swTile = GetBattleTile(new Vector2Int(tile.Coordinates.x - distance, tile.Coordinates.y - distance));
                if (swTile != null && (!walkableOnly || (swTile.IsWalkable))) neighbors.Add(swTile);
            
                BattleTile nwTile = GetBattleTile(new Vector2Int(tile.Coordinates.x - distance, tile.Coordinates.y + distance));
                if (nwTile != null && (!walkableOnly || (nwTile.IsWalkable))) neighbors.Add(nwTile);
            }

            return neighbors;
        }

        /// <summary>
        /// Returns an integer representing how many tiles are between tileA and tileB.
        /// </summary>
        /// <returns></returns>
        public int GetDistanceInTiles(BattleTile tileA, BattleTile tileB)
        {
            //Since this implementation of A* doesn't take diagonals into account,
            //we can just use a simple Manhattan distance formula instead.
            return Mathf.Abs(tileA.Coordinates.x - tileB.Coordinates.x) + Mathf.Abs(tileA.Coordinates.y - tileB.Coordinates.y);
        }

        public List<Vector2Int?> AllLerpDestinations()
        {
            return FindObjectsOfType<GridMovement>().Select(gridMov => gridMov.LerpDestination).ToList();
        }

        public List<BattleTile> OccupiedTiles()
        {
            List<BattleTile> occupiedTiles = new List<BattleTile>();

            foreach (Unit u in BattleManager.Instance.UnitsInRoom)
            {
                occupiedTiles.Add(GetBattleTile(u.CurrentCoord));
            }
            return occupiedTiles;
        }
    }
}