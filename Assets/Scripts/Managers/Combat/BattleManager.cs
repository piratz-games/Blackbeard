using System;
using System.Collections.Generic;
using System.Linq;
using Abstractions.Combat;
using Debugging;
using Helpers;
using MonoBehaviours.Combat;
using MonoBehaviours.Combat.AI;
using MonoBehaviours.Combat.UI.Menu;
using Sirenix.OdinInspector;
using UnityEngine;
using UnityEngine.Events;

namespace Managers.Combat
{
    public enum DefenseMode
    {
        Block,
        Dodge,
        Parry,
        StaySentry
    }

    public enum BattleTeams
    {
        Ally,
        Enemy,
        Environment
    }

    public class BattleManager : Singleton<BattleManager>
    {
        public Camera cam;
        
        private InputManager inputManager;

        [HideInInspector]
        public bool inBattle = false;

        [HideInInspector]
        public bool transition = false;

        public class BattleEvent : UnityEvent { };
        public readonly BattleEvent OnBattleStart = new BattleEvent();
        public readonly BattleEvent OnBattleEnd = new BattleEvent();
        
        public class BattleUnitEvent : UnityEvent<Unit> { };
        public readonly BattleUnitEvent OnTurnStart = new BattleUnitEvent();
        public readonly BattleUnitEvent OnTurnEnd = new BattleUnitEvent();

        public List<BaseWindowManager> windowManagers;

        [HideInInspector]
        public List<Unit> alliesInRoom;
        public List<Unit> enemiesInRoom;
        public List<Unit> UnitsInRoom => alliesInRoom.Concat(enemiesInRoom).ToList();

        private Unit currentUnit;
        private BattleTeams currentTeam;
        private List<BattleTeams> teamOrder;
        private int teamOrderIndex = 0;

        public Unit PlayerUnit => alliesInRoom.FirstOrDefault(p => p.isMainCharacter);

        [BoxGroup("Aim Markers")] public GameObject tileHighlight;
        [BoxGroup("Aim Markers")] public GameObject tileCrosshair;
        [BoxGroup("Aim Markers")] public Color 
            fullDamageColor = Color.red, 
            halfDamageColor = new Color(1f, 0.5f, 0f), 
            quarterDamageColor = Color.yellow;

        private void Awake()
        {
            inputManager = new InputManager();

            inputManager.Debug.Enable();
            inputManager.Debug.ToggleBattle.performed += (context =>
            {
                if(!DebugManager.Instance.debugMode) return;
                
                if (inBattle)
                    EndBattle();
                else
                    NewBattle(PlayerUnit);
            });

            NewRoom();
        }

        private void OnDisable()
        {
            inputManager.Debug.Disable();
            inputManager.Debug.ToggleBattle.performed -= (context =>
            {
                if(!DebugManager.Instance.debugMode) return;
                
                if (inBattle)
                    EndBattle();
                else
                    NewBattle(PlayerUnit);
            });
        }

        private void NewRoom()
        {
            alliesInRoom = new List<Unit>();
            enemiesInRoom = new List<Unit>();

            alliesInRoom.AddRange(FindObjectsOfType<Unit>().Where(u => u.faction == BattleFactions.Ally).OrderByDescending(u => u.initiative));
            enemiesInRoom.AddRange(FindObjectsOfType<Unit>().Where(u => u.faction == BattleFactions.Enemy).OrderByDescending(u => u.initiative));

            foreach (Unit u in UnitsInRoom)
            {
                u.OnHealthDepleted.AddListener(() => DefeatUnit(u));
            }
        }

        public void NewBattle(Unit startingUnit)
        {
            if (NoEnemiesLeft) return;
            if (inBattle) return;

            inBattle = true;
            
            OnBattleStart?.Invoke();
            
            foreach (Unit u in UnitsInRoom)
            {
                u.InitForBattle();
                u.finishedTurn = false;
            }
            
            teamOrder = startingUnit.faction == BattleFactions.Enemy
                ? new List<BattleTeams>() {BattleTeams.Enemy, BattleTeams.Ally, BattleTeams.Environment}
                : new List<BattleTeams>() {BattleTeams.Ally, BattleTeams.Enemy, BattleTeams.Environment};
            currentTeam = teamOrder[0];
            teamOrderIndex = 0;

            currentUnit = startingUnit;

            transition = true;

            GridManager.Instance.FadeIn(() =>
            {
                transition = false;
            });
            
            OnTurnStart?.Invoke(currentUnit);

            if (!currentUnit.IsPlayable)
            {
                currentUnit.GetComponent<EnemyBattleAI>().TurnStart();
            }
        }

        private void EndBattle()
        {
            inBattle = false;
            transition = true;
            
            foreach (Unit u in UnitsInRoom)
            {
                u.DisableAfterBattle();
            }

            GridManager.Instance.FadeOut(() =>
            {
                transition = false;
                OnBattleEnd?.Invoke();
            });
        }

        public bool CanAct(Unit unit)
        {
            return (inBattle && !transition && !windowManagers.Any(w => w.MenuOpened && w.locksControl) && currentUnit.GetInstanceID() == unit.GetInstanceID());
        }

        public bool PlayableTurn()
        {
            return (currentUnit != null && currentUnit.IsPlayable);
        }

        public List<Vector2Int> OccupiedTiles(BattleFactions? faction = null)
        {
            return (from u in UnitsInRoom
                where !faction.HasValue || u.faction == faction.Value
                select u.CurrentCoord).ToList();
        }
        
        public List<Unit> UnitsInTile(Vector2Int coord)
        {
            return UnitsInRoom.Where(u => u.CurrentCoord == coord).ToList();
        }

        public void EndTurn(bool defending)
        {
            if (!inBattle) return;

            foreach (BaseWindowManager manager in windowManagers)
            {
                manager.CloseAllMenus();
            }

            if (currentTeam != BattleTeams.Environment)
            {
                if(defending) currentUnit.SetDefense(DefenseMode.Block);
                OnTurnEnd?.Invoke(currentUnit);
                currentUnit.GetComponent<GridMovement>().SnapToGrid();
            }

            currentUnit.finishedTurn = true;
            NextTurn();
        }

        private void NextTurn()
        {
            if (currentTeam == BattleTeams.Environment)
            {
                Debug.Log("Environment Plays");
                SwitchTeams();
                return;
            }
            
            Unit nextUnit = currentTeam == BattleTeams.Ally
                ? alliesInRoom.Where(a => !a.finishedTurn).OrderByDescending(a => a.initiative).FirstOrDefault()
                : enemiesInRoom.Where(e => !e.finishedTurn).OrderByDescending(e => e.initiative).FirstOrDefault();

            if (nextUnit == null)
            {
                SwitchTeams();
            }
            else
            {
                currentUnit = nextUnit;

                OnTurnStart?.Invoke(currentUnit);

                if (!currentUnit.IsPlayable)
                {
                    currentUnit.GetComponent<EnemyBattleAI>().TurnStart();
                }
            }
        }

        private void SwitchTeams()
        {
            teamOrderIndex++;

            if (teamOrderIndex >= teamOrder.Count) teamOrderIndex = 0;

            currentTeam = teamOrder[teamOrderIndex];

            switch (currentTeam)
            {
                case BattleTeams.Ally:
                {
                    foreach (Unit ally in alliesInRoom)
                    {
                        ally.finishedTurn = false;
                    }

                    break;
                }
                case BattleTeams.Enemy:
                {
                    foreach (Unit enemy in enemiesInRoom)
                    {
                        enemy.finishedTurn = false;
                    }

                    break;
                }
                case BattleTeams.Environment:
                    break;
                default:
                    throw new ArgumentOutOfRangeException();
            }

            Debug.Log("Turn Start: " + currentTeam.ToString());
            
            NextTurn();
        }

        private void DefeatUnit(Unit unit)
        {
            if(unit.faction == BattleFactions.Ally) alliesInRoom.Remove(unit);
            else enemiesInRoom.Remove(unit);
            unit.gameObject.SetActive(false);

            if (unit.isMainCharacter)
            {
                Debug.Log("Game Over!");
                if (inBattle) EndBattle();
                return;
            }
            
            if (NoEnemiesLeft)
            {
                if (inBattle) EndBattle();
            }
            else
            {
                if (transition) transition = false;
            }
        }

        private bool NoEnemiesLeft => enemiesInRoom.Count <= 0;
    }
}
