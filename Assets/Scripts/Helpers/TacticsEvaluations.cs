using System;
using System.Collections.Generic;
using System.Linq;
using Abstractions.AI;
using Abstractions.Combat;
using Managers.Combat;
using UnityEngine;

namespace Helpers
{
    public static class TacticsEvaluations
    {
        /// <summary>
        /// Evaluates if a given TacticTrigger evaluates to TRUE in the current moment.
        /// </summary>
        /// <param name="t"></param>
        /// <param name="caller"></param>
        /// <returns>TRUE if triggered, FALSE otherwise</returns>
        public static bool EvaluateTrigger(TacticTrigger t, Unit caller)
        {
            switch (t.target)
            {
                case TriggerTarget.Always:
                    return true;
                case TriggerTarget.ThisUnitHP:
                {
                    float healthPercentage = Mathf.InverseLerp(0, caller.maxHp,
                        caller.currentHp);
                    return EvaluateResult(healthPercentage, t.percentageResult, t.comparison);
                }
                case TriggerTarget.LeaderHP:
                {
                    float healthPercentage = Mathf.InverseLerp(0, BattleManager.Instance.PlayerUnit.maxHp, BattleManager.Instance.PlayerUnit.currentHp);
                    return EvaluateResult(healthPercentage, t.percentageResult, t.comparison);
                }
                case TriggerTarget.NearestEnemyHP:
                {
                    Unit nearestEnemy = GetNearestEnemy(caller);
                    if (nearestEnemy == null) return false;

                    float healthPercentage = Mathf.InverseLerp(0, nearestEnemy.maxHp, nearestEnemy.currentHp);
                    return EvaluateResult(healthPercentage, t.percentageResult, t.comparison);
                }
                case TriggerTarget.TilesFromLeader:
                {
                    float dist = TileDistance(BattleManager.Instance.PlayerUnit.CurrentCoord, caller.CurrentCoord);
                    return EvaluateResult(dist, t.integerResult, t.comparison);
                }
                case TriggerTarget.TilesFromNearestEnemy:
                {
                    Unit nearestEnemy = GetNearestEnemy(caller);
                    if (nearestEnemy == null) return false;

                    float dist = TileDistance(nearestEnemy.CurrentCoord, caller.CurrentCoord);
                    return EvaluateResult(dist, t.integerResult, t.comparison);
                }
                case TriggerTarget.EnemyInWeaponRange:
                {
                    Unit nearestEnemy = GetNearestEnemy(caller);
                    if (nearestEnemy == null) return false;

                    float dist = Vector2.Distance(nearestEnemy.transform.position, caller.transform.position);
                    return dist <= caller.currentWeapon.range.y && dist >= caller.currentWeapon.range.x;
                }
                case TriggerTarget.HealingInMoveRange:
                    return false;
                default:
                    return false;
            }
        }

        private static bool EvaluateResult(float value, float expected, TriggerComparison comparison)
        {
            return comparison switch
            {
                TriggerComparison.Equal => (Math.Abs(value - expected) < 0.1f),
                TriggerComparison.Less => (value < expected),
                TriggerComparison.LessOrEqual => (value <= expected),
                TriggerComparison.Greater => (value > expected),
                TriggerComparison.GreaterOrEqual => (value >= expected),
                var t => false
            };
        }

        private static Unit GetNearestEnemy(Unit caller)
        {
            List<Unit> enemies = caller.faction == BattleFactions.Ally
                ? BattleManager.Instance.enemiesInRoom
                : BattleManager.Instance.alliesInRoom;

            Unit closestEnemy = enemies.OrderBy(c => Vector3.Distance(caller.transform.position, c.transform.position))
                .FirstOrDefault();
            return closestEnemy;
        }
        
        private static int TileDistance(Vector2Int a, Vector2Int b)
        {
            //Manhattan distance formula
            return Mathf.Abs(a.x - b.x) + Mathf.Abs(a.y - b.y);
        }
    }
}