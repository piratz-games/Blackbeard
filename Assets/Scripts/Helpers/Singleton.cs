﻿using Sirenix.OdinInspector;
using UnityEngine;

namespace Helpers
{
    public class Singleton<T> : MonoBehaviour where T : Component
    {
        private static T instance;

        public static T Instance
        {
            get
            {
                if (instance == null)
                {
                    T[] existingInstances = (T[]) FindObjectsOfType(typeof(T));

                    if (existingInstances.Length > 1)
                        Debug.LogWarning(typeof(T).Name + "instance is not unique. Make sure to remove any duplicates from the Scene.");

                    if (existingInstances.Length > 0)
                    {
                        //Uses the same Singleton instance already in the scene.
                        instance = existingInstances[0];
                    }  
                    else
                    {
                        GameObject gameObj = new GameObject();

                        //Avoids saving this GameObject in the Scene view, creating "ghost" instances.
                        gameObj.hideFlags = HideFlags.HideAndDontSave;
                        instance = gameObj.AddComponent<T>();
                    }
                }
                return instance;
            }
        }
    }
}