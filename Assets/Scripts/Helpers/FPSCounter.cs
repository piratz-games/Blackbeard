using TMPro;
using UnityEngine;

namespace Helpers
{
    [RequireComponent(typeof(TMP_Text))]
    public class FPSCounter : MonoBehaviour
    {
        private TMP_Text thisText;

        private void Awake()
        {
            thisText = GetComponent<TMP_Text>();
        }

        private void LateUpdate()
        {
            thisText.text = "" + (int)(1 / Time.smoothDeltaTime);
        }
    }
}
