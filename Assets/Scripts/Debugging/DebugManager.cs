using Helpers;
using Sirenix.OdinInspector;
using UnityEngine;
using UnityEngine.Events;

namespace Debugging
{
    [ExecuteInEditMode]
    public class DebugManager : Singleton<DebugManager>
    {
        [HideInInspector, OnValueChanged("DebugModeChanged")]
        public bool debugMode;

        public Canvas debugCanvas;
        public UnityEngine.Experimental.Rendering.Universal.PixelPerfectCamera pixelPerfect;
        public Camera cam;

        public class DebugEvent : UnityEvent<bool> { }
        public DebugEvent OnDebugModeChanged;
        
        private void DebugModeChanged()
        {
            OnDebugModeChanged?.Invoke(debugMode);
        }

        [ShowIf("debugMode")]
        [Button(ButtonSizes.Gigantic), GUIColor(1, 1, 0)]
        private void DebugModeON()
        {
            debugMode = false;
            pixelPerfect.enabled = true;
            cam.orthographicSize = 5.625f;
            debugCanvas.enabled = false;
        }
    
        [HideIf("debugMode")]
        [Button(ButtonSizes.Gigantic)]
        private void DebugModeOFF()
        {
            debugMode = true;
            pixelPerfect.enabled = false;
            cam.orthographicSize = 8.66f;
            debugCanvas.enabled = true;
        }
    }
}
