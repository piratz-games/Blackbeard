using Abstractions.Combat;
using TMPro;
using UnityEngine;

namespace Debugging
{
    public class UnitDebugUI : MonoBehaviour
    {
        public Canvas thisCanvas;
        public TMP_Text nameTxt;
        public TMP_Text hpTxt;
        public TMP_Text apTxt;

        private Unit thisUnit;

        private void Awake()
        {
            Transform thisTransform = transform.parent;
            thisUnit = thisTransform.GetComponent<Unit>();
            nameTxt.text = thisTransform.name;
        }

        private void Update()
        {
            thisCanvas.enabled = DebugManager.Instance.debugMode;

            hpTxt.text = $"HP: {thisUnit.currentHp}/{thisUnit.maxHp}";
            apTxt.text = $"AP: {thisUnit.currentAp}/{thisUnit.maxAp}";
        }
    }
}
