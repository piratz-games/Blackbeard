﻿using System.Collections.Generic;
using System.Linq;
using Managers.Combat;
using Sirenix.Utilities;

namespace Abstractions
{
    /// <summary>
    /// This class implements the A* algorithm using tilemap data from the battle grid.
    /// </summary>
    public static class AStarPathfinding
    {
        private static BattleTile lowestHCostTile = null;
        
        public static Stack<BattleTile> FindPath(BattleTile startTile, BattleTile targetTile,
            BattleTile[] exclusions = null)
        {
            lowestHCostTile = null;
            
            //A list of tiles that can be explored to find a path.
            List<BattleTile> openSet = new List<BattleTile>();

            //A hashset of tiles that have been already explored.
            //Since we can't have duplicate cells on it, we use a HashSet instead of a List.
            HashSet<BattleTile> closedSet = new HashSet<BattleTile>();

            openSet.Add(startTile);
            if (exclusions != null) closedSet.AddRange(exclusions);

            while (openSet.Count > 0)
            {
                //Finds a open node with the lowest F-Cost.
                BattleTile currentTile = openSet[0];
                for (int i = 1; i < openSet.Count; i++) //Skips index 0 because it's the current cell.
                {
                    if (openSet[i].Pathfinding.FCost < currentTile.Pathfinding.FCost ||
                        (openSet[i].Pathfinding.FCost == currentTile.Pathfinding.FCost &&
                            openSet[i].Pathfinding.HCost < currentTile.Pathfinding.HCost))
                    {
                        currentTile = openSet[i];
                    }
                }
                
                openSet.Remove(currentTile);
                closedSet.Add(currentTile);

                if (lowestHCostTile == null || lowestHCostTile.Pathfinding.HCost > currentTile.Pathfinding.HCost) lowestHCostTile = currentTile;

                if (currentTile.Coordinates == targetTile.Coordinates)
                {
                    //Path found.
                    return RetracePath(startTile, targetTile);
                }
                else
                {
                    //Path not found yet. Loop through neighbors. Excludes neighbors that can't be walkable.
                    foreach (BattleTile ne in GridManager.Instance.GetTileNeighbors(currentTile, 1,true))
                    {
                        //Skips if the neighboring tile has already been calculated.
                        if (closedSet.Contains(ne))
                        {
                            continue;
                        }

                        //Checks if a new neighbor with less cost has appeared;
                        int newCost = currentTile.Pathfinding.GCost + GridManager.Instance.GetDistanceInTiles(currentTile, ne);

                        if (newCost < ne.Pathfinding.GCost || !openSet.Contains(ne))
                        {
                            ne.Pathfinding.GCost = newCost;
                            ne.Pathfinding.HCost = GridManager.Instance.GetDistanceInTiles(ne, targetTile);
                            ne.Pathfinding.ParentTile = currentTile;

                            if (!openSet.Contains(ne))
                            {
                                openSet.Add(ne);
                            }
                        }
                    }
                }
            }

            if (lowestHCostTile != null)
            {
                return lowestHCostTile.Coordinates != startTile.Coordinates ? RetracePath(startTile, lowestHCostTile) : null;
            }

            return null;
        }

        /// <summary>
        /// Traces backwards the path taken to find the shortest distance by using the
        /// tile's parent references.
        /// </summary>
        /// <param name="startTile"></param>
        /// <param name="endTile"></param>
        private static Stack<BattleTile> RetracePath(BattleTile startTile, BattleTile endTile)
        {
            Stack<BattleTile> path = new Stack<BattleTile>();
            BattleTile currentTile = endTile;

            while (currentTile != startTile)
            {
                path.Push(currentTile);
                currentTile = currentTile.Pathfinding.ParentTile;
            }

            return path;
        }
    }
}