using UnityEngine.Events;

namespace Abstractions.Combat
{
    public enum ActionType
    {
        None,
        Move,
        Attack,
        Shoot,
        Item,
        Skill,
        Wait
    }

    public class BattleStep
    {
        //public int hpCost = 0;
        //public int apCost = 0;

        public delegate void ActionDelegate();
        public ActionDelegate AdvanceAction;
        public ActionDelegate UndoAction;

        protected class ActionCompleteEvent: UnityEvent { };
        protected ActionCompleteEvent AdvanceComplete = new ActionCompleteEvent();
        protected ActionCompleteEvent UndoComplete = new ActionCompleteEvent();

        public ActionType ActionType;
    }
}