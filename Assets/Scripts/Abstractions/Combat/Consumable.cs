﻿using System;
using Sirenix.OdinInspector;
using UnityEngine;

namespace Abstractions.Combat
{
    [CreateAssetMenu(fileName = "Assets/ScriptableObjects/Consumables/New Consumable", menuName = "Blackbeard/Consumable")]
    [Serializable]
    public class Consumable : Item
    {
        [BoxGroup("Properties")]
        public int healing;
    }
}