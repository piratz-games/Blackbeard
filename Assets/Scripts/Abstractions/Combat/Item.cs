using Sirenix.OdinInspector;
using UnityEngine;

namespace Abstractions.Combat
{
    public abstract class Item : SerializedScriptableObject
    {
        [Multiline]
        public string description;
        [BoxGroup("Properties")]
        public int apCost = 1;
        
        [BoxGroup("Durability", Order = 50)]
        [Tooltip("Determines if this weapon breaks after [durability] times.")]
        public bool disposable = false;
        
        [ShowIf("disposable")]
        [BoxGroup("Durability")]
        [Range(1, 5)] public int durability = 1;
        public bool Disposed => timesUsed >= (ulong) durability;
        
        [HideInInspector]
        public ulong timesUsed = 0;
    }
}