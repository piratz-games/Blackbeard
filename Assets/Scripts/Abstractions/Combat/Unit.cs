using System;
using System.Linq;
using Debugging;
using DG.Tweening;
using Managers;
using Managers.Combat;
using MonoBehaviours.Combat;
using MonoBehaviours.Combat.AI;
using Sirenix.OdinInspector;
using UnityEngine;
using UnityEngine.Events;

namespace Abstractions.Combat
{
    public enum BattleFactions
    {
        Ally,
        Enemy
    }
    
    public class Unit : BattleEntity
    {
        [ShowIf("@this.faction == BattleFactions.Ally")]
        public bool isMainCharacter = false;
        public BattleFactions faction = BattleFactions.Enemy;
        [ShowInInspector, ReadOnly]
        public bool IsPlayable => GetComponent<PlayerMovement>();

        [HideInInspector]
        public bool finishedTurn = false;

        [Title("Stats")]
        [Min(1)]
        public int maxHp = 5;
        [ShowInInspector, ReadOnly]
        public int currentHp;
        [Min(1)]
        public int maxAp = 6;
        [Min(1)]
        public int apRechargeAmount = 4;
        [ShowInInspector, ReadOnly]
        [Min(0)]
        public int currentAp;
        public int currentDefense = 3; //TODO: Add DEFENSE stats to weapons and equipment, and sum them all in this line.
        [Min(0)]
        public int initiative = 1;
        [Min(0)]
        public int moveCost = 1;

        private int reservedAP = 0;
        private DefenseMode currentDefenseMode;
        public bool defending = false;

        public class BattleUnitEvent : UnityEvent { };

        private readonly BattleUnitEvent onDamageTaken = new BattleUnitEvent();
        public readonly BattleUnitEvent OnHealthDepleted = new BattleUnitEvent();
        private readonly BattleUnitEvent onHealthChanged = new BattleUnitEvent();
        private readonly BattleUnitEvent onFullHealth = new BattleUnitEvent();
        private readonly BattleUnitEvent onApDepleted = new BattleUnitEvent();
        private readonly BattleUnitEvent onApRestored = new BattleUnitEvent();
        public readonly BattleUnitEvent OnNoActionsLeft = new BattleUnitEvent();
        
        private Inventory inventory;
        
        [HideInInspector]
        public Weapon currentWeapon;

        public bool CanAct => BattleManager.Instance.CanAct(this);

        public bool HasEnoughAp(int cost)
        {
            return currentAp - cost >= 0;
        }

        public int CheapestAction =>
            Mathf.Min(moveCost, currentWeapon ? currentWeapon.apCost : int.MaxValue,
                inventory.items?.Min(c => c.apCost) ?? int.MaxValue);

        protected override void Awake()
        {
            base.Awake();

            currentHp = maxHp;
            currentAp = maxAp;

            BattleManager.Instance.OnBattleStart.AddListener(() => RestoreAP(99999));
            BattleManager.Instance.OnBattleEnd.AddListener(DisableAfterBattle);
            BattleManager.Instance.OnTurnStart.AddListener(BeginTurn);
            BattleManager.Instance.OnTurnEnd.AddListener(FinishTurn);

            inventory = GetComponent<Inventory>();
        }

        protected override void OnDestroy()
        {
            BattleManager.Instance.OnBattleStart.RemoveListener(() => RestoreAP(99999));
            BattleManager.Instance.OnBattleEnd.RemoveListener(DisableAfterBattle);
            BattleManager.Instance.OnTurnStart.RemoveListener(BeginTurn);
            BattleManager.Instance.OnTurnEnd.RemoveListener(FinishTurn);
            base.OnDestroy();
        }

        public virtual void InitForBattle() { }

        public virtual void DisableAfterBattle() { }

        public void AttemptAction(System.Action act, int cost)
        {
            if (!CanAct) return;
            if (!HasEnoughAp(moveCost))
            {
                OnNoActionsLeft?.Invoke();
                return;
            }
            act.Invoke();
        }

        public void SpendAP(int cost)
        {
            if (cost <= 0 || !CanAct) return;
            currentAp = Mathf.Max(currentAp - cost, 0);
            if (currentAp <= 0) onApDepleted?.Invoke();
        }

        public void RestoreAP(int cost)
        {
            if (cost <= 0) return;
            currentAp = Mathf.Min(maxAp, currentAp + cost);
            onApRestored?.Invoke();
        }

        public void TakeDamage(int damage)
        {
            int originalDamage = damage;
            
            bool hitSoundPlayed = false;
            
            GetComponent<AIAiming>()?.StopAllCoroutines();
            GetComponent<RadialAimingSystem>()?.EndAiming();
            
            //Reduce damage by defense value. Reserved AP is added to defense if current End Mode is Block.
            if (defending)
            {
                switch (currentDefenseMode)
                {
                    case DefenseMode.Block:
                    case DefenseMode.Dodge:
                    case DefenseMode.StaySentry:
                    {
                        damage = Mathf.Max(0, Mathf.Max(1, damage - (currentDefense + reservedAP)));
                        
                        SfxManager.Instance.PlaySfx(SoundEffects.Block, ASource);
                        hitSoundPlayed = true;
                        
                        if (DebugManager.Instance.debugMode)
                            Debug.Log($"{gameObject.name} blocked with {reservedAP} extra AP. {originalDamage} DMG - ({currentDefense} DEF + {reservedAP} Block AP) = {damage} DMG");
                        break;
                    }
                    case DefenseMode.Parry:
                    {
                        damage = 0;
                
                        SfxManager.Instance.PlaySfx(SoundEffects.Parry, ASource);
                        hitSoundPlayed = true;

                        if(DebugManager.Instance.debugMode) Debug.Log($"{gameObject.name} parried! No damage.");
                        break;
                    }
                    default:
                        throw new ArgumentOutOfRangeException();
                }
                reservedAP = Mathf.Max(0, reservedAP - 1);
            }
            else
            {
                damage = Mathf.Max(0, Mathf.Max(1, damage - currentDefense));
                if(DebugManager.Instance.debugMode) Debug.Log($"{gameObject.name} didn't block. {originalDamage} DMG - {currentDefense} DEF = {damage} DMG");
            }


            if (damage > 0)
            {
                currentHp -= damage;

                onDamageTaken?.Invoke();
                if(!hitSoundPlayed) SfxManager.Instance.PlaySfx(SoundEffects.Hurt, ASource);
            }
            
            if (currentHp <= 0) BattleManager.Instance.transition = true;
            
            transform.DOKill();
            Transform sprite = spriteRend.transform;
            sprite.DOKill();
            sprite.localPosition = Vector3.zero;
            sprite.DOShakePosition(0.2f, Vector3.one * 0.6f, 50).OnComplete(() =>
            {
                if (currentHp > 0) return;

                spriteRend.transform.DOKill();
                currentHp = 0;
                OnHealthDepleted?.Invoke();

                //if (counterStacks > 0) CounterAttack((int)damage/2);
            });
        }

        public void Heal(int amount)
        {
            if (amount <= 0) return;

            currentHp += amount;
            onHealthChanged?.Invoke();

            if (currentHp >= maxHp)
            {
                currentHp = maxHp;
                onFullHealth?.Invoke();
            }
        }
        
        public void UndoHeal(int amount)
        {
            if (amount <= 0) return;

            currentHp -= amount;
            onHealthChanged?.Invoke();
           
            if (currentHp <= 0) currentHp = 1;
        }

        private void BeginTurn(Unit unitInTurn)
        {
            if (unitInTurn != this) return;
            RestoreAP(defending ? apRechargeAmount + reservedAP : apRechargeAmount);
            reservedAP = 0;
        }

        public void SetDefense(DefenseMode defMode)
        {
            currentDefenseMode = defMode;
            reservedAP = currentAp;
            currentAp = 0;
        }
        
        private void FinishTurn(Unit unitInTurn)
        {
            if (unitInTurn != this) return;
            //TODO: process parry and counter stacks here.
        }
    }
}