﻿using System;
using Sirenix.OdinInspector;
using UnityEngine;

namespace Abstractions.Combat
{
    [Flags]
    public enum WeaponEquipSlot
    {
        Any = 0,
        Main = 1 << 0,
        Sub = 1 << 1,
    }

    [Flags]
    public enum WeaponElements
    {
        None = 0,
        Fire = 1 << 0,
        Water = 1 << 1,
        Earth = 1 << 2,
        Air = 1 << 3
    }

    [CreateAssetMenu(fileName = "Assets/ScriptableObjects/Weapons/New Weapon", menuName = "Blackbeard/Weapon")]
    [Serializable]
    public class Weapon : Item
    {
        [BoxGroup("Properties")]
        public int damage = 1;

        [HideIf("disposable")]
        [BoxGroup("Properties")]
        public WeaponEquipSlot equipSlot;

        [BoxGroup("Ranges"), MinMaxSlider(0, 10, true), OnValueChanged("ValidateRanges")]
        public Vector2 range = new Vector2(0, 4);

        [BoxGroup("Ranges"), Range(1f, 10), OnValueChanged("ValidateRanges")]
        public float damageArea = 1;

        [BoxGroup("Weapon Attributes")]
        public WeaponElements elements;

        [BoxGroup("Weapon Attributes")]
        [Tooltip("Determines if this weapon's range arches over small walls. Otherwise, small walls will block it.")]
        public bool archTrajectory = false;

        [BoxGroup("Weapon Attributes")]
        [Tooltip("Determines if this weapon's damage area only applies for units over floors.")]
        public bool requiresFloor = false;

        [BoxGroup("Weapon Attributes")]
        [Tooltip("Determines if this weapon's damage area pass through walls.")]
        public bool wallPenetration = false;

        [BoxGroup("Weapon Attributes")]
        [Tooltip("Determines if this weapon's damage area pass through units.")]
        public bool unitPenetration = false;


        private void ValidateRanges()
        {
            range.x = (float) Math.Round(range.x * 2, MidpointRounding.AwayFromZero) / 2;
            range.y = (float) Math.Round(range.y * 2, MidpointRounding.AwayFromZero) / 2;
            damageArea = (float) Math.Round(damageArea * 2, MidpointRounding.AwayFromZero) / 2;

            if (damageArea <= 1f && range.y < 1.5f) range.y = 1.5f;
        }
    }
}