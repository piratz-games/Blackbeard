using Managers.Combat;
using Sirenix.OdinInspector;
using UnityEngine;
using UnityEngine.Events;

namespace Abstractions.Combat
{
    [RequireComponent(typeof(AudioSource))]
    public abstract class BattleEntity : MonoBehaviour
    {
        public SpriteRenderer spriteRend;

        public class BattleEntityEvent : UnityEvent { }
        public readonly BattleEntityEvent OnActionFinish = new BattleEntityEvent();

        protected AudioSource ASource;

        [ReadOnly]
        public Vector2Int CurrentCoord => (Vector2Int) GridManager.Instance.tilemap.WorldToCell(transform.position);

        protected virtual void Awake()
        {
            BattleStepManager.Instance.OnNewAction.AddListener(() => OnStep?.Invoke());
            
            ASource = GetComponent<AudioSource>();
        }

        protected virtual void OnDestroy()
        {
            BattleStepManager.Instance.OnNewAction.RemoveListener(() => OnStep?.Invoke());
        }

        protected delegate void StepAction();

        protected StepAction OnStep;
    }
}