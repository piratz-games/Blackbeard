using System;
using Sirenix.OdinInspector;
using UnityEngine;

namespace Abstractions.Dungeon
{
    public enum DungeonNodeTypes
    {
        Default,
        Entrance,
        Boss,
        PathToBoss
    }
    
    public class DungeonNode
    {
        public Vector2Int Coord;
        public RoomExits Exits;
        public DungeonNodeTypes Type;
        public bool HasPathToStart = false;

        public DungeonNode(Vector2Int c)
        {
            Coord = c;
        }
    }
}
