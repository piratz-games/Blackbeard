using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using JetBrains.Annotations;
using Sirenix.OdinInspector;
using UnityEngine;
using UnityEngine.Tilemaps;

namespace Abstractions.Dungeon
{
    public class BiomeMapGenerator : MonoBehaviour
    {
        [OnValueChanged("GenerateBtn")]
        public Vector2Int mapSize = new Vector2Int(10, 10);
        
        private DungeonNode[,] dungeons;
        
        [PropertyRange(2, "MaxDungeons"), OnValueChanged("GenerateBtn")]
        public int dungeonAmount = 10;
        [Tooltip("How many dungeon blocks should exist in the path between the starting dungeon (exclusive) and the boss dungeon (inclusive)."), 
         PropertyRange(2, "dungeonAmount"),  OnValueChanged("GenerateBtn")]
        public int dungeonsToBoss = 5;

        private int MaxDungeons => Mathf.RoundToInt((mapSize.x) * (mapSize.y));
        private List<DungeonNode> addedBlocks = new List<DungeonNode>();
        
        public Tilemap debugMap;

        [Header("Debug Tiles")]
        public Tile tileNone;
        public Tile startTile, bossTile;

        [BoxGroup("Tile Connectors")]
        public Tile tileN, tileE, tileS, tileW, tileNE, tileSE, tileSW, tileNW, tileNWE, tileNSE, tileSWE, tileNSW, tileNS, tileWE, tileNSEW;

        private bool generating = false;
        [UsedImplicitly] private string GenButtonLabel => generating ? "Generating..." : "Regenerate Map";

        private List<DungeonNode> pathToBoss = null;
        
        void OnEnable()
        {
            GenerateBtn();
        }

        [Button ("$GenButtonLabel"), DisableIf("generating")]
        public void GenerateBtn()
        {
            StopAllCoroutines();
            generating = true;
            
            //Trim roomAmount in case it surpasses the available slots in dungeonSize.
            if (dungeonAmount > MaxDungeons) dungeonAmount = MaxDungeons;

            StartCoroutine(GenerateBiomeMap());
        }

        IEnumerator GenerateBiomeMap()
        {
            debugMap.ClearAllTiles();
            yield return new WaitForEndOfFrame();
            dungeons = new DungeonNode[(int)mapSize.x, (int)mapSize.y];
            addedBlocks = new List<DungeonNode>();
            dungeons[(int) mapSize.x / 2, (int) mapSize.y / 2] = new DungeonNode(new Vector2Int((int)mapSize.x/2, (int)mapSize.y/2));
            DungeonNode currentNode = dungeons[(int) mapSize.x / 2, (int) mapSize.y / 2];
            addedBlocks.Add(currentNode);
            
            for (int i = 0; i < dungeonAmount - 1; i++)
            {
                float randomPerc = (float) i / ((float) dungeonAmount - 1);
                float randomCompare = Mathf.Lerp(0.2f, 0.01f, randomPerc);
                Vector2Int? nextPos = NewDungeonPos();
                //Test new position
                if (nextPos == null || (GetNeighbors(nextPos.Value).Count > 1 && UnityEngine.Random.value > randomCompare))
                {
                    int j = 0;
                    do
                    {
                        nextPos = NewDungeonPos();
                        j++;
                        yield return new WaitForEndOfFrame();
                        if (j >= 1000) break;
                    }
                    while (nextPos == null || GetNeighbors(nextPos.Value).Count > 1);
                }

                if (!nextPos.HasValue)
                {
                    StartCoroutine(GenerateBiomeMap());
                }
                else
                {
                    //Finish positioning
                    DungeonNode newNode = new DungeonNode(nextPos.Value);
                    dungeons[(int) nextPos.Value.x, (int) nextPos.Value.y] = newNode;
                    addedBlocks.Add(newNode);
                }
            }
            
            SetStartAndEndBlocks();
        }
        
        private Vector2Int? NewDungeonPos()
        {
            Vector2Int newCoord;
            int tries = 1000;
            do
            {
                int index = (int) (UnityEngine.Random.value * (addedBlocks.Count - 1));
                int newX = (int) addedBlocks[index].Coord.x;
                int newY = (int) addedBlocks[index].Coord.y;
                bool horizontal = UnityEngine.Random.value < 0.5f;
                bool positive = UnityEngine.Random.value < 0.5f;

                if (horizontal)
                {
                    newX = positive ? newX+1 : newX-1;
                }
                else
                {
                    newY = positive ? newY+1 : newY-1;
                }

                newCoord = new Vector2Int(newX, newY);
                tries--;

                if (tries <= 0) return null;

            }
            while (addedBlocks.Any(dr => dr.Coord == newCoord) || OutOfBounds(newCoord));

            return newCoord;
        }

        private void SetStartAndEndBlocks()
        {
            DungeonNode boss = addedBlocks[addedBlocks.Count - 1];
            boss.Type = DungeonNodeTypes.Boss;

            DungeonNode entrance = addedBlocks.Where(b=> GetPathToNode(b.Coord, boss.Coord)?.Count == dungeonsToBoss)
                .OrderBy(b => UnityEngine.Random.value)
                .FirstOrDefault();

            if (entrance == null)
            {
                StartCoroutine(GenerateBiomeMap()); //Recursively regenerates a dungeon until a boss room within the given distance from the start can be found.
                return;
            }

            entrance.Type = DungeonNodeTypes.Entrance;

            pathToBoss = GetPathToNode(entrance.Coord, boss.Coord).Where(b => b.Type == DungeonNodeTypes.Default).ToList();
            
            foreach (DungeonNode db in pathToBoss)
            {
                db.Type = DungeonNodeTypes.PathToBoss;
                SetConnections(db, pathToBoss);
                db.HasPathToStart = true;
            }
            
            Draw();
        }

        private void SetConnections(DungeonNode db, ICollection<DungeonNode> nodesToConnect)
        {
            if (db.Coord.y + 1 < dungeons.GetLength(1))
            {
                DungeonNode north = dungeons[db.Coord.x, db.Coord.y + 1];
                if (north != null && nodesToConnect.Contains(north)) db.Exits |= RoomExits.North;
            }

            if (db.Coord.y -1 >= 0)
            {
                DungeonNode south = dungeons[db.Coord.x, db.Coord.y - 1];
                if (south != null && nodesToConnect.Contains(south)) db.Exits |= RoomExits.South;
            }

            if (db.Coord.x + 1 < dungeons.GetLength(0))
            {
                DungeonNode east = dungeons[db.Coord.x + 1, db.Coord.y];
                if (east != null && nodesToConnect.Contains(east)) db.Exits |= RoomExits.East;
            }

            if (db.Coord.x -1 >= 0)
            {
                DungeonNode west = dungeons[db.Coord.x - 1, db.Coord.y];
                if (west != null && nodesToConnect.Contains(west)) db.Exits |= RoomExits.West;
            }
        }

        void Draw()
        {
            debugMap.ClearAllTiles();

            transform.position = new Vector3Int(-mapSize.x/2, -mapSize.y/2, 0);

            foreach (DungeonNode blo in addedBlocks)
            {
                switch (blo.Type)
                {
                    case DungeonNodeTypes.Default:
                        debugMap.SetTile((Vector3Int)blo.Coord, tileNone);
                        break;
                    case DungeonNodeTypes.PathToBoss:
                        debugMap.SetTile((Vector3Int)blo.Coord, PickExits(blo.Exits));
                        debugMap.SetTileFlags((Vector3Int)blo.Coord, TileFlags.None);
                        debugMap.SetColor((Vector3Int)blo.Coord, new Color(1f, 0.5f, 0f));
                        break;
                    case DungeonNodeTypes.Entrance:
                        debugMap.SetTile((Vector3Int)blo.Coord, startTile);
                        break;
                    case DungeonNodeTypes.Boss:
                        debugMap.SetTile((Vector3Int)blo.Coord, bossTile);
                        break;
                    default:
                        throw new ArgumentOutOfRangeException();
                }
            }
            
            generating = false;
        }

        private TileBase PickExits(RoomExits bloExits)
        {
            switch (bloExits)
            {
                case RoomExits.North:
                    return tileN;
                case RoomExits.South:
                    return tileS;
                case RoomExits.East:
                    return tileE;
                case RoomExits.West:
                    return tileW;
                
                case RoomExits.North | RoomExits.East:
                    return tileNE;
                case RoomExits.South | RoomExits.East:
                    return tileSE;
                case RoomExits.South | RoomExits.West:
                    return tileSW;
                case RoomExits.North | RoomExits.West:
                    return tileNW;
                
                case RoomExits.North | RoomExits.South:
                    return tileNS;
                case RoomExits.East | RoomExits.West:
                    return tileWE;
                
                case RoomExits.None:
                default:
                    return tileNone;
            }
        }

        #region Helpers
        private List<DungeonNode> GetNeighbors(Vector2 pos)
        {
            List<DungeonNode> neighbors = new List<DungeonNode>();

            if (addedBlocks.Any(dr => dr.Coord == pos + Vector2.right))
                neighbors.Add(addedBlocks.FirstOrDefault(dr => dr.Coord == pos + Vector2.right));
            if (addedBlocks.Any(dr => dr.Coord == pos + Vector2.left))
                neighbors.Add(addedBlocks.FirstOrDefault(dr => dr.Coord == pos + Vector2.left));
            if (addedBlocks.Any(dr => dr.Coord == pos + Vector2.up))
                neighbors.Add(addedBlocks.FirstOrDefault(dr => dr.Coord == pos + Vector2.up));
            if (addedBlocks.Any(dr => dr.Coord == pos + Vector2.down))
                neighbors.Add(addedBlocks.FirstOrDefault(dr => dr.Coord == pos + Vector2.down));
            
            return neighbors;
        }
        
        private int GetBlockDistance(Vector2Int coordA, Vector2Int coordB)
        {
            return Mathf.Abs(coordA.x - coordB.x) + Mathf.Abs(coordA.y - coordB.y);
        }

        private Stack<DungeonNode> GetPathToNode(Vector2Int start, Vector2Int end)
        {
            Stack<DungeonNode> exploredCoords = new Stack<DungeonNode>();
            DungeonNode currentNode = addedBlocks.FirstOrDefault(ab => ab.Coord == start);
            if (currentNode == null) throw new Exception();
            exploredCoords.Push(currentNode);

            while (currentNode.Coord != end)
            {
                DungeonNode nearestNeighbor = (GetNeighbors(currentNode.Coord).Except(exploredCoords)).OrderBy(n => GetBlockDistance(n.Coord, end)).FirstOrDefault();
                if (nearestNeighbor == null) return null; 
                exploredCoords.Push(nearestNeighbor);
                currentNode = nearestNeighbor;
            }

            return exploredCoords;
        }
        
        bool OutOfBounds(Vector2 vec)
        {
            return vec.x >= mapSize.x || vec.x < 0 || vec.y >= mapSize.y || vec.y < 0;
        }
        #endregion
        
       
    }
}
