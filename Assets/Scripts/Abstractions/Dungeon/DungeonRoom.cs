using System;
using Sirenix.OdinInspector;
using UnityEngine;

namespace Abstractions.Dungeon
{
    [Flags]
    public enum RoomExits
    {
        None = 0,
        North = 1 << 0,
        South = 1 << 1,
        East = 1 << 2,
        West = 1 << 3
    }

    public enum RoomTypes
    {
        Default,
        Start,
        RestPoint
    }
    
    [CreateAssetMenu(fileName = "Assets/ScriptableObjects/Dungeon/New Room Set", menuName = "Blackbeard/Dungeon/Room Set")]
    [Serializable]
    public class DungeonRoom : SerializedScriptableObject
    {
        public GameObject roomPrefab;
        public RoomTypes type;
        public RoomExits exits;
    }
}