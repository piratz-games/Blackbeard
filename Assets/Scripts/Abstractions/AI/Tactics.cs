﻿using System;
using System.Collections.Generic;
using Managers.Combat;
using Sirenix.OdinInspector;
using UnityEngine;

namespace Abstractions.AI
{

    public enum TriggerTarget
    {
        Always,
        ThisUnitHP,
        LeaderHP,
        NearestEnemyHP,
        TilesFromLeader,
        TilesFromNearestEnemy,
        EnemyInWeaponRange,
        HealingInMoveRange
        
    }
    
    public enum TriggerComparison
    {
        Equal,
        Less,
        LessOrEqual,
        Greater,
        GreaterOrEqual
    };
    
    public enum TacticAction
    {
        Rest,
        AttackLeader,
        AttackNearestEnemy,
        Defend,
        Evade,
    }
    
    [Serializable]
    public struct Tactic
    {
        public List<TacticTrigger> triggers;
        public TacticAction action;
    }

    [Serializable]
    public struct TacticTrigger
    {
        public TriggerTarget target;

        [HideIf("@this.target == TriggerTarget.EnemyInWeaponRange || " +
                "this.target == TriggerTarget.HealingInMoveRange || " +
                "this.target == TriggerTarget.Always")]
        public TriggerComparison comparison;

        [Range(0f, 1f)]
        [ShowIf("@this.target == TriggerTarget.LeaderHP || " + 
                "this.target == TriggerTarget.ThisUnitHP || " +
                "this.target == TriggerTarget.NearestEnemyHP")]
        public float percentageResult;
        
        [ShowIf("@this.target == TriggerTarget.TilesFromLeader || " + 
                "this.target == TriggerTarget.TilesFromNearestEnemy")]
        [MinValue(0), MaxValue(50)]
        public int integerResult;
        
        [ShowIf("@this.target == TriggerTarget.EnemyInWeaponRange || " + 
                "this.target == TriggerTarget.HealingInMoveRange")]
        public bool boolResult;
    }
    
    [CreateAssetMenu(fileName = "Assets/ScriptableObjects/AI Tactics/New AI Tactics", menuName = "Blackbeard/AI Tactics")]
    [Serializable]
    public class Tactics : SerializedScriptableObject
    {
        public List<Tactic> priorityList;
        [Tooltip("FALSE if Unit prefers to Rest when ending a turn. TRUE if the Unit prefer to Block, Dodge, Parry or Stay Sentry when ending a turn.")]
        public bool defendsOnTurnEnd;
    }
}