﻿using UnityEngine;
using UnityEngine.Tilemaps;

namespace Abstractions {

    public class BattleTile
    {
        /// <summary>
        /// Coordinates in grid system.
        /// </summary>
        public Vector2Int Coordinates;

        public bool IsWalkable = true;

        /// <summary>
        /// Stores pathfinding cost information to be used by the A* algorithm.
        /// </summary>
        public PathCosts Pathfinding;

        /// <summary>
        /// Property that returns this battleTileData's tile in Unity's Tilemap engine.
        /// </summary>
        /// <returns></returns>
        public TileBase Tile;
    }

    #region Cell Structs
    public struct PathCosts
    {
        /// <summary>
        /// Represents the movement cost of going from the starting cell to this one.
        /// </summary>
        public int GCost;
        /// <summary>
        /// Represents the Heuristic cost of this cell, with an estimation of the distance to the target.
        /// </summary>
        public int HCost;

        /// <summary>
        /// The Final Cost, which is simply gCost + hCost.
        /// </summary>
        public int FCost => GCost + HCost;

        /// <summary>
        /// In pathfinding terms, this represents the cell that was been traversed before reaching this one.
        /// </summary>
        public BattleTile ParentTile;
    }
    #endregion
}