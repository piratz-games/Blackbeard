using System.Collections;
using System.Collections.Generic;
using Abstractions;
using Abstractions.Combat;
using Debugging;
using DG.Tweening;
using Managers.Combat;
using MonoBehaviours.Combat;
using MonoBehaviours.Combat.AI;
using UnityEngine;

namespace MonoBehaviours.Overworld
{
    public enum OverworldAIState
    {
        Wandering,
        Chasing
    }

    [RequireComponent(typeof(AIGridMovement))]
    [RequireComponent(typeof(AIAiming))]
    public class EnemyOverworldAI : MonoBehaviour
    {
        private AIGridMovement gridMov;
        private AIAiming aiming;
        private Unit thisUnit;
        
        private BattleTile wanderGoal;

        public SpriteRenderer debugWanderPoint;

        private OverworldAIState currentState;
        public float detectionRange = 6f;

        private void Awake()
        {
            gridMov = GetComponent<AIGridMovement>();
            aiming = GetComponent<AIAiming>();
            thisUnit = GetComponent<Unit>();
            
            BattleManager.Instance.OnBattleStart.AddListener(()=>
            {
                KillTweensAndCoroutines();
                gridMov.SnapToGrid();
            });
            
            BattleManager.Instance.OnBattleEnd.AddListener(Wander);
        }
        
        private void OnEnable()
        {
            Wander();
        }

        private void Update()
        {
            if (BattleManager.Instance.PlayerUnit == null) return;

            if (currentState != OverworldAIState.Chasing && PlayerInFoV())
            {
                KillTweensAndCoroutines();
                Chase();
            }
            
            debugWanderPoint.transform.position = currentState == OverworldAIState.Wandering
                ? GridManager.Instance.tilemap.GetCellCenterWorld((Vector3Int) wanderGoal.Coordinates)
                : GridManager.Instance.tilemap.GetCellCenterWorld((Vector3Int) BattleManager.Instance.PlayerUnit.CurrentCoord);
            
            debugWanderPoint.enabled = DebugManager.Instance.debugMode && !BattleManager.Instance.inBattle;
        }

        private bool PlayerInFoV()
        {
            Vector3 playerPos = BattleManager.Instance.PlayerUnit.transform.position;
            Vector3 thisPos = this.transform.position;
            
            bool facingPlayer = gridMov.currentDirection switch
            {
                Direction.North => playerPos.y > thisPos.y,
                Direction.East => playerPos.x > thisPos.x,
                Direction.South => playerPos.y < thisPos.y,
                Direction.West => playerPos.x < thisPos.x,
                var _ => false
            };
            return facingPlayer && Vector2.Distance(playerPos, thisPos) <= detectionRange;
        }

        #region Wander
        private void Wander()
        {
            currentState = OverworldAIState.Wandering;
            
            do
            {
                wanderGoal = GridManager.Instance.GetRandomBattleTile();
            }
            while (!wanderGoal.IsWalkable);
            
            WanderTowardsGoal();
            debugWanderPoint.color = Color.magenta;
        }
        
        private void WanderTowardsGoal()
        {
            BattleTile thisTile = GridManager.Instance.GetBattleTile(thisUnit.CurrentCoord);

            Stack<BattleTile> foundPath = AStarPathfinding.FindPath(thisTile, wanderGoal);
            if(foundPath == null) Debug.LogError("Invalid Wander Target. Coord: " + wanderGoal.Coordinates);
            if (foundPath != null && foundPath.Count > 0)
            {
                if (!this.enabled) return;
                BattleTile nextTile = foundPath.Pop();

                if (foundPath.Count == 0 && BattleManager.Instance.OccupiedTiles().Contains(wanderGoal.Coordinates))
                {
                    debugWanderPoint.color = Color.yellow;
                    StartCoroutine(Idle());
                }
                else
                {
                    gridMov.WanderToCoord(nextTile.Coordinates, GridManager.Instance.moveDelay * 2, WanderTowardsGoal);
                }
            }
            else
            {
                debugWanderPoint.color = foundPath == null? Color.red : Color.cyan;
                StartCoroutine(Idle());
            }
        }
        #endregion

        #region Chase
        private void Chase()
        {
            currentState = OverworldAIState.Chasing;
            debugWanderPoint.color = Color.red;

            if (BattleManager.Instance.PlayerUnit == null)
            {
                Wander();
                return;
            }
            
            BattleTile thisTile = GridManager.Instance.GetBattleTile(thisUnit.CurrentCoord);
            BattleTile playerTile = GridManager.Instance.GetBattleTile(BattleManager.Instance.PlayerUnit.CurrentCoord);
            
            bool nearestEnemyInAttackRange =
                Vector2.Distance(thisUnit.transform.position, BattleManager.Instance.PlayerUnit.transform.position) <=
                thisUnit.currentWeapon.range.y;
            
            if (!this.enabled || BattleManager.Instance.inBattle) return;

            if (!nearestEnemyInAttackRange)
            {
                Stack<BattleTile> foundPath = AStarPathfinding.FindPath(thisTile, playerTile);
                if(foundPath == null) Debug.LogError("Invalid Chase Target. Coord: " + playerTile.Coordinates);
                if (foundPath != null && foundPath.Count > 0)
                {
                    BattleTile nextTile = foundPath.Pop();

                    if (foundPath.Count == 0 && BattleManager.Instance.OccupiedTiles().Contains(playerTile.Coordinates))
                    {
                        debugWanderPoint.color = Color.yellow;
                        StartCoroutine(ChaseWait());
                    }
                    else
                    {
                        gridMov.WanderToCoord(nextTile.Coordinates, 0.4f, Chase);
                    }
                }
                else
                {
                    debugWanderPoint.color = foundPath == null? Color.gray : Color.cyan;
                    StartCoroutine(Idle());
                }
            }
            else
            {
                aiming.AttackTile(playerTile.Coordinates, Chase);
            }

        }

        #endregion
        
        private IEnumerator Idle()
        {
            wanderGoal = GridManager.Instance.GetBattleTile(thisUnit.CurrentCoord);
            yield return new WaitForSeconds(UnityEngine.Random.Range(2f, 5f));
            if (!this.enabled) yield return null;
            Wander();
        }

        private IEnumerator ChaseWait()
        {
            yield return new WaitForEndOfFrame();
            if (!this.enabled) yield return null;
            Chase();
        }
        
        private void KillTweensAndCoroutines()
        {
            transform.DOKill();
            GetComponent<AIAiming>()?.EndAiming();
            StopAllCoroutines();
        }
    }
}