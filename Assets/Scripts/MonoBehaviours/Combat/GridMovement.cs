using System.Collections.Generic;
using System.Linq;
using Abstractions;
using Abstractions.Combat;
using DG.Tweening;
using Managers.Combat;
using Sirenix.OdinInspector;
using UnityEngine;

namespace MonoBehaviours.Combat
{
    public enum Direction
    {
        None,
        North,
        East,
        South,
        West
    }
    
    public class GridMovement : MonoBehaviour
    {
        protected BattleEntity ThisEntity;
        private Stack<Vector2Int> pathHistory;
        protected int OngoingLerps { get; private set; }

        public Vector2Int? LerpDestination = null;

        [ReadOnly]
        public Direction currentDirection;

        protected virtual void Awake()
        {
            ThisEntity = GetComponent<BattleEntity>();
            OngoingLerps = 0;

            BattleManager.Instance.OnBattleStart.AddListener(() =>
            {
                pathHistory = new Stack<Vector2Int>();
                SnapToGrid();
            });
            
            BattleManager.Instance.OnTurnEnd.AddListener((u) =>
            {
                pathHistory = new Stack<Vector2Int>();
                BattleStepManager.Instance.ClearUndoStack();
            });

            currentDirection = Direction.None;
        }

        public void SnapToGrid()
        {
            Vector2Int snapTarget = ThisEntity.CurrentCoord;
            List<Unit> unitsInSameTile = BattleManager.Instance.UnitsInTile(ThisEntity.CurrentCoord);
            
            if (unitsInSameTile.Count > 1 && unitsInSameTile[0].gameObject.GetInstanceID() != ThisEntity.gameObject.GetInstanceID())
            {
                BattleTile thisTile = GridManager.Instance.GetBattleTile(ThisEntity.CurrentCoord);
                
                int i = 1;
                do
                {
                    snapTarget = GridManager.Instance.GetTileNeighbors(thisTile, i, true, true)
                        .Where(bt => !GridManager.Instance.AllLerpDestinations().Contains(bt.Coordinates))
                        .OrderBy(bt => Vector2.Distance(transform.position, GridManager.Instance.tilemap.GetCellCenterWorld((Vector3Int) bt.Coordinates)))
                        .FirstOrDefault()
                        .Coordinates;
                    i++;
                }
                while (snapTarget == ThisEntity.CurrentCoord);
            }

            currentDirection = Direction.None;
            LerpToCoord(snapTarget);
        }

        //TODO: Add more conditions that block movement, such as enemies in the way or hazards.
        protected bool CanMoveTo(Vector2Int pos)
        {
            Vector2Int targetPos = new Vector2Int(ThisEntity.CurrentCoord.x + pos.x, ThisEntity.CurrentCoord.y + pos.y);
            BattleTile targetTile = GridManager.Instance.GetBattleTile(targetPos);
            
            if(targetTile == null) return false;
            return !BattleManager.Instance.OccupiedTiles().Contains(targetPos) && targetTile.IsWalkable;
        }

        public void MoveInput(Vector2Int pos)
        {
            ChangeDirection(pos);
            LerpToCoord(new Vector2Int(ThisEntity.CurrentCoord.x + pos.x, ThisEntity.CurrentCoord.y + pos.y), true);
        }

        public void UndoMove()
        {
            if (pathHistory.Count <= 0) return;

            Vector2Int lastPath = pathHistory.Pop();
            
            ChangeDirection(ThisEntity.CurrentCoord - lastPath);
            LerpToCoord(lastPath);
        }

        protected void LerpToCoord(Vector2Int destination, bool includeInPathHistory = false)
        {
            if (includeInPathHistory) AddPosToPathHistory(ThisEntity.CurrentCoord);
            OngoingLerps++;

            LerpDestination = destination;

            transform.DOMove(GridManager.Instance.tilemap.GetCellCenterWorld((Vector3Int)destination), GridManager.Instance.moveDelay).SetEase(Ease.OutExpo).OnComplete(() =>
            {
                ThisEntity.OnActionFinish?.Invoke();
                OngoingLerps--;
                OngoingLerps = Mathf.Max(OngoingLerps, 0);
                LerpDestination = null;
            });
        }

        public void WanderToCoord(Vector2Int destination, float duration, System.Action callback)
        {
            ChangeDirection(destination);
            
            transform.DOMove(GridManager.Instance.tilemap.GetCellCenterWorld((Vector3Int) destination), duration).SetEase(Ease.Linear)
                .OnComplete(() => callback?.Invoke());
        }

        protected void AddPosToPathHistory(Vector2Int pos)
        {
            pathHistory.Push(pos);
        }

        private void ChangeDirection(Vector2Int delta)
        {
            if (delta.x == 0)
            {
                currentDirection = delta.y > 0 ? Direction.North : Direction.South;
            }
            else
            {
                currentDirection = delta.x > 0 ? Direction.East : Direction.West;
            }
        }
    }
}