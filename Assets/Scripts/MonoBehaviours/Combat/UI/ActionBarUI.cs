using System.Collections.Generic;
using System.Linq;
using Abstractions.Combat;
using DG.Tweening;
using Managers;
using Managers.Combat;
using UnityEngine;

namespace MonoBehaviours.Combat.UI
{
    public class ActionBarUI : MonoBehaviour
    {
        public Transform actionSlotsParent;
        public GameObject actionSlot;
        public RectTransform animRect;

        public void Awake()
        {
            BattleManager.Instance.OnTurnEnd.AddListener((x) => ClearSlots());

            foreach (Unit unit in FindObjectsOfType<Unit>())
            {
                //if (unit.OnActionTaken == null) unit.OnActionTaken = new BattleUnit.TurnActionsEvent();
                //unit.OnActionTaken?.AddListener(UpdateSlots);
            }
        }

        public void UpdateSlots(IEnumerable<Abstractions.Combat.BattleStep> actions)
        {
            ClearSlots();

            foreach (GameObject slot in actions.Select(act => Instantiate(actionSlot, actionSlotsParent))) { }
        }

        private void ClearSlots()
        {
            foreach (Transform child in actionSlotsParent.transform)
            {
                GameObject.Destroy(child.gameObject);
            }
        }

        public void ShakeBar()
        {
            animRect.DOKill(true);
            animRect.DOShakeAnchorPos(0.25f, Vector3.one * 2f, 50);
            SfxManager.Instance.PlaySfx(SoundEffects.UI_Unable);
        }
        
    }
}
