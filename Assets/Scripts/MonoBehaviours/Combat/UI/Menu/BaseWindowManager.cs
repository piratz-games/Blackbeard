using System;
using System.Linq;
using UnityEngine;

namespace MonoBehaviours.Combat.UI.Menu {
    
    [Serializable]
    public struct MenuPanel
    {
        public string type;
        public GameObject panel;
    }

    public abstract class BaseWindowManager : MonoBehaviour
    {
        public bool locksControl = true;
        public float panelFadeDelay = 0.2f;
        public MenuPanel[] panels;
        protected string CurrentMenu = string.Empty;
        protected InputManager InputManager;

        public bool MenuOpened => !string.IsNullOrEmpty(CurrentMenu);

        protected virtual void Awake()
        {
            InputManager = new InputManager();
            InputManager.Menu.Enable();
        }

        protected virtual void OnDisable()
        {
            InputManager.Menu.Disable();
        }

        protected void OpenMenu(string type)
        {
            if (string.IsNullOrEmpty(type)) return;

            GameObject panel = panels.FirstOrDefault(x => x.type == type).panel;
            if (panel == null) return;

            if(!panel.activeSelf) panel.SetActive(true);
            panel.GetComponent<IFadeableUI>().FadeIn();
            CurrentMenu = type;
        }

        protected void CloseMenu(string type)
        {
            if (string.IsNullOrEmpty(CurrentMenu)) return;

            GameObject panel = panels.FirstOrDefault(x => x.type == type).panel;
            if (panel == null) return;

            if(panel.activeSelf) panel.GetComponent<IFadeableUI>().FadeOut();
            CurrentMenu = string.Empty;
        }

        public void CloseAllMenus(bool instant = false)
        {
            foreach (MenuPanel menu in panels.Where(menu => menu.panel.activeSelf))
            {
                if (instant)
                {
                    menu.panel.SetActive(false);
                }
                else
                {
                    menu.panel.GetComponent<IFadeableUI>().FadeOut();
                }
            }
        }
    }
}