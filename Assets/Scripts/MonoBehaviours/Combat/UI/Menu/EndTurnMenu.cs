using DG.Tweening;
using Managers.Combat;
using UnityEngine;

namespace MonoBehaviours.Combat.UI.Menu
{
    public class EndTurnMenu : MonoBehaviour, IFadeableUI
    {
        public PromptWindowManager promptManager;
        public BattleManager battleManager;
        private InputManager inputManager;

        private void Awake()
        {
            inputManager = new InputManager();
            inputManager.Player.Enable();
            
            inputManager.Player.EndTurnRest.performed += (context =>
            {
                if (!battleManager.PlayableTurn()) return;
                battleManager.EndTurn(false);
            });
            inputManager.Player.EndTurnDefend.performed += (context =>
            {
                if (!battleManager.PlayableTurn()) return;
                battleManager.EndTurn(true);
            });

            transform.localScale = Vector3.zero;
        }

        public void FadeIn()
        {
            transform.DOKill();
            transform.DOScale(Vector3.one, promptManager.panelFadeDelay).SetEase(Ease.OutQuad);
        }

        public void FadeOut()
        {
            transform.DOKill();
            transform.DOScale(Vector3.zero, promptManager.panelFadeDelay).SetEase(Ease.OutQuad).OnComplete(() =>
            {
                gameObject.SetActive(false);
            });
        }
    }
}
