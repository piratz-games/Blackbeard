using System.Linq;
using Abstractions.Combat;
using Managers.Combat;

namespace MonoBehaviours.Combat.UI.Menu
{
    public class PromptWindowManager : BaseWindowManager
    {
        public BattleManager battleManager;
        public ActionBarUI apBar;
        
        protected override void Awake()
        {
            base.Awake();
            
            foreach (Unit playables in FindObjectsOfType<Unit>().Where(u => u.IsPlayable))
            {
                playables.OnNoActionsLeft.AddListener(apBar.ShakeBar);
            }
            
            InputManager.Player.Enable();

            InputManager.Player.EndTurn.performed += (context =>
            {
                if (!battleManager.PlayableTurn()) return;
                if (string.IsNullOrEmpty(CurrentMenu))
                {
                    OpenMenu("End Turn");
                }
            });
            
            InputManager.Player.EndTurn.canceled += (context =>
            {
                if (!battleManager.PlayableTurn()) return;
                if (CurrentMenu == "End Turn")
                {
                    CloseMenu("End Turn");
                }
            });
        }

        protected override void OnDisable()
        {
            foreach (Unit playables in FindObjectsOfType<Unit>().Where(u => u.IsPlayable))
            {
                //playables.OnApDepleted.RemoveListener(ShakeApBar);
                playables.OnNoActionsLeft.RemoveListener(apBar.ShakeBar);
            }
            
            InputManager.Player.EndTurn.performed -= (context =>
            {
                if (!battleManager.PlayableTurn()) return;
                if (string.IsNullOrEmpty(CurrentMenu))
                {
                    OpenMenu("End Turn");
                }
            });
            
            InputManager.Player.EndTurn.canceled -= (context =>
            {
                if (!battleManager.PlayableTurn()) return;
                if (CurrentMenu == "End Turn")
                {
                    CloseMenu("End Turn");
                }
            });
            
            base.OnDisable();
        }
    }
}