namespace MonoBehaviours.Combat.UI.Menu
{
    public interface IFadeableUI
    {
        public void FadeIn();
        public void FadeOut();
        
    }
}