using UnityEngine;

[RequireComponent(typeof(LineRenderer))]
public class LineRendererPolygon : MonoBehaviour
{
    [Range(3, 100)]
    public int vertexes = 3;
    public float width = 0.5f;
    private LineRenderer lineRend;

    public void DrawRadius(float radius)
    {
        lineRend = GetComponent<LineRenderer>();
        
        lineRend.enabled = true;
        
        lineRend.startWidth = width;
        lineRend.endWidth = width;
        lineRend.loop = true;
        float angle = 2 * Mathf.PI / vertexes;
        lineRend.positionCount = vertexes;

        for (int i = 0; i < vertexes; i++)
        {
            Matrix4x4 rotationMatrix = new Matrix4x4(new Vector4(Mathf.Cos(angle * i), Mathf.Sin(angle * i), 0, 0),
                new Vector4(-1 * Mathf.Sin(angle * i), Mathf.Cos(angle * i), 0, 0),
                new Vector4(0, 0, 1, 0),
                new Vector4(0, 0, 0, 1));
            Vector3 initialRelativePosition = new Vector3(0, radius, 0);
            lineRend.SetPosition(i,rotationMatrix.MultiplyPoint(initialRelativePosition));
        }
    }

    public void Clear()
    {
        lineRend = GetComponent<LineRenderer>();
        lineRend.enabled = false;
    }
}
