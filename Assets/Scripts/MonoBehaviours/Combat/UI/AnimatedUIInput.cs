using System.Collections;
using UnityEngine;
using UnityEngine.UI;

namespace MonoBehaviours.Combat.UI
{
    [RequireComponent(typeof(Image))]
    public class AnimatedUIInput : MonoBehaviour
    {
        public Sprite[] sprites;
        public float delay = 0.5f;
        private Image img;

        // Start is called before the first frame update
        void OnEnable()
        {
            img = GetComponent<Image>();
            StartCoroutine(AnimLoop());
        }

        IEnumerator AnimLoop()
        {
            for (int i = 0; i < sprites.Length; i++)
            {
                img.sprite = sprites[i];
                yield return new WaitForSeconds(delay);
            }

            StartCoroutine(AnimLoop());
        }
    }
}
