using System.Collections.Generic;
using System.Linq;
using Abstractions.Combat;
using Debugging;
using DG.Tweening;
using Managers.Combat;
using MonoBehaviours.Combat.AI;
using UnityEngine;
using UnityEngine.Events;

namespace MonoBehaviours.Combat
{
    public abstract class RadialAimingSystem : MonoBehaviour
    {
        private Unit thisUnit;

        public class WeaponAttackEvent : UnityEvent { };

        public readonly WeaponAttackEvent OnAim = new WeaponAttackEvent();
        public readonly WeaponAttackEvent OnAttack = new WeaponAttackEvent();
        private readonly WeaponAttackEvent onAimedTileChange = new WeaponAttackEvent();

        public Transform aim;
        public Transform cursor;

        public LineRendererPolygon maxRangeLine, minRangeLine;
        public LineRendererPolygon damageRadius;
        
        private List<GameObject> highlightMarkers;

        private bool isAiming;

        private Vector2Int currentAimedCoord;

        protected const float TileRadius = .5f;
        private const float MinRangeToAim = 1f;

        protected bool IsAiming
        {
            get => isAiming;
            set
            {
                if (isAiming == value) return;

                isAiming = value;

                if (value)
                {
                    StartAiming();
                }
                else
                {
                    EndAiming();
                }
            }
        }

        protected virtual void Awake()
        {
            thisUnit = GetComponent<Unit>();
            BattleStepManager.Instance.OnUndoAction.AddListener(()=> IsAiming = false);
            
            BattleManager.Instance.OnBattleEnd.AddListener(EndAiming);

            aim.transform.localPosition = Vector3.zero;
            damageRadius.transform.localScale = Vector3.zero;

            IsAiming = false;
            
            onAimedTileChange.AddListener(() =>            
            {
                ResetDamageTiles();
                DrawDamageTiles(currentAimedCoord);
            });

            currentAimedCoord = new Vector2Int(9999, 9999);
        }
        
        protected virtual void OnDestroy()
        {
            BattleStepManager.Instance.OnUndoAction.RemoveListener(() => IsAiming = false);
            onAimedTileChange.RemoveListener(() =>
            {
                ResetDamageTiles();
                DrawDamageTiles(currentAimedCoord);
            });
            
            BattleManager.Instance.OnBattleEnd.RemoveListener(EndAiming);
        }
        
        protected void DigitalAim(Vector2 input)
        {
            if (!isAiming) return;

            //Get rid of diagonal movement.
            if (input.x != 0 && input.y != 0) input = Vector2.zero;
            if (input.magnitude == 0) return;
            
            Vector2 targetPos = ((Vector2)aim.transform.position + (input * GridManager.Instance.tilemap.cellSize.x));

            if (Vector2.Distance(transform.position, targetPos) >= (thisUnit.currentWeapon.range.y - TileRadius) + (GridManager.Instance.tilemap.cellSize.x/2)) targetPos = ((Vector2)aim.transform.position);

            Vector3Int nextTile = GridManager.Instance.tilemap.WorldToCell(targetPos);
            aim.transform.position = GridManager.Instance.tilemap.GetCellCenterWorld(nextTile);
            
            cursor.transform.position = targetPos;
            
            CheckForAimedTileChange();
        }

        protected void AnalogAim(Vector2 input)
        {
            if (!isAiming) return;

            Vector2 targetPos = input * (thisUnit.currentWeapon.range.y - (TileRadius + .1f));
            
            cursor.transform.localPosition = targetPos;
            
            if (targetPos.magnitude <= thisUnit.currentWeapon.range.x - (TileRadius/2))
                targetPos = targetPos.normalized * (thisUnit.currentWeapon.range.x - (TileRadius/2));
            
            aim.transform.localPosition = thisUnit.currentWeapon.range.y > MinRangeToAim ? targetPos : Vector2.zero;
            
            CheckForAimedTileChange();
        }

        protected void MouseAim(Vector2 mousePos)
        {
            if (!isAiming) return;
            
            Vector3 targetPos = Vector3.ClampMagnitude(
                (Vector3) mousePos - GridManager.Instance.tilemap.GetCellCenterWorld((Vector3Int) thisUnit.CurrentCoord),
                (thisUnit.currentWeapon.range.y - (TileRadius + .1f)));

            cursor.transform.localPosition = targetPos;
            
            if (targetPos.magnitude <= thisUnit.currentWeapon.range.x - (TileRadius/2))
               targetPos = targetPos.normalized * (thisUnit.currentWeapon.range.x - (TileRadius/2));

            aim.transform.localPosition = thisUnit.currentWeapon.range.y > MinRangeToAim ? targetPos : Vector3.zero;
            
            CheckForAimedTileChange();
        }

        protected void TileAim(Vector2Int tile)
        {
            if (!isAiming) return;

            Vector3 targetPos = GridManager.Instance.tilemap.GetCellCenterWorld((Vector3Int)tile);
            
            cursor.transform.position = targetPos;
            aim.transform.position = targetPos;

            currentAimedCoord = (Vector2Int) GridManager.Instance.tilemap.WorldToCell(aim.transform.position);
            ResetDamageTiles();
            DrawDamageTiles(currentAimedCoord);
        }

        private void StartAiming()
        {
            thisUnit.GetComponent<GridMovement>().SnapToGrid();
            if(thisUnit.IsPlayable) thisUnit.GetComponent<PlayerMovement>().enabled = false;

            maxRangeLine.DrawRadius(thisUnit.currentWeapon.range.y - TileRadius);
            maxRangeLine.transform.DOScale(Vector3.one, 0.15f).SetEase(Ease.OutCirc);

            if (thisUnit.currentWeapon.range.x - TileRadius > 0f)
            {
                minRangeLine.DrawRadius(thisUnit.currentWeapon.range.x - TileRadius);
                minRangeLine.transform.DOScale(Vector3.one, 0.15f).SetEase(Ease.OutCirc);
            }
            
            if (thisUnit.currentWeapon.damageArea > 1)
            {
                damageRadius.DrawRadius(thisUnit.currentWeapon.damageArea - TileRadius);
            }
            else
            {
                damageRadius.Clear();
            }
            
            damageRadius.transform.DOScale(Vector3.one, 0.15f).SetEase(Ease.OutCirc);
            cursor.transform.DOScale(Vector3.one, 0.15f).SetEase(Ease.OutCirc);

            highlightMarkers = new List<GameObject>();

            currentAimedCoord = (Vector2Int) GridManager.Instance.tilemap.WorldToCell(aim.transform.position);
            onAimedTileChange?.Invoke();
        }
        
        public void EndAiming()
        {
            if(aim != null) aim.transform.localPosition = Vector3.zero;
            if(cursor != null) cursor.transform.localPosition = Vector3.zero;
            
            if(thisUnit.IsPlayable) thisUnit.GetComponent<PlayerMovement>().enabled = true;
            
            maxRangeLine.transform.DOScale(Vector3.zero, 0.15f);
            minRangeLine.transform.DOScale(Vector3.zero, 0.15f);
            damageRadius.transform.DOScale(Vector3.zero, 0.15f).OnComplete(() => damageRadius.Clear());
            cursor.transform.DOScale(Vector3.zero, 0.15f).SetEase(Ease.OutCirc);
            
            ResetDamageTiles();
        }
        
        private void CheckForAimedTileChange()
        {
            if (currentAimedCoord != (Vector2Int) GridManager.Instance.tilemap.WorldToCell(aim.transform.position))
            {
                currentAimedCoord = (Vector2Int) GridManager.Instance.tilemap.WorldToCell(aim.transform.position);
                onAimedTileChange?.Invoke();
            }
        }

        private void DrawDamageTiles(Vector2Int originTile)
        {
            Vector3 originMiddle = GridManager.Instance.tilemap.GetCellCenterWorld((Vector3Int) originTile);
            
            if (highlightMarkers != null) ResetDamageTiles();
            highlightMarkers = new List<GameObject>();
            
            float fullDamageDist = thisUnit.currentWeapon.damageArea - TileRadius;
            float halfDamageDist = thisUnit.currentWeapon.damageArea - 0.01f;
            
            GameObject reticule = Instantiate(BattleManager.Instance.tileCrosshair, GridManager.Instance.tilemap.transform);
            highlightMarkers.Add(reticule);
            reticule.transform.position = originMiddle;

            foreach (Vector3Int tileCoord in GridManager.Instance.tilemap.cellBounds.allPositionsWithin)
            {
                if (thisUnit.CurrentCoord == (Vector2Int)tileCoord) continue;

                Vector3 targetMiddle = GridManager.Instance.tilemap.GetCellCenterWorld(tileCoord);

                if (Vector3.Distance(originMiddle, targetMiddle) <= halfDamageDist)
                {
                    GameObject mark = Instantiate(BattleManager.Instance.tileHighlight, GridManager.Instance.tilemap.transform);
                    highlightMarkers.Add(mark);
                    mark.transform.position = targetMiddle;
                    
                    SpriteRenderer sr = mark.GetComponent<SpriteRenderer>();
                    
                    //Color accordingly to the tile's damage output.
                    Vector3 unitPos = GridManager.Instance.tilemap.GetCellCenterWorld((Vector3Int) thisUnit.CurrentCoord);
                    if (Vector3.Distance(unitPos, originMiddle) > thisUnit.currentWeapon.range.y - TileRadius)
                    {
                        sr.color = Vector3.Distance(originMiddle, targetMiddle) > fullDamageDist
                            ? BattleManager.Instance.quarterDamageColor
                            : BattleManager.Instance.halfDamageColor;
                    }
                    else
                    {
                        sr.color = Vector3.Distance(originMiddle, targetMiddle) > fullDamageDist
                            ? BattleManager.Instance.halfDamageColor
                            : BattleManager.Instance.fullDamageColor;
                    }
                    
                    Sequence seq = DOTween.Sequence(mark);
                    seq.Append(sr.DOFade(0f, 0.5f));
                    seq.Append(sr.DOFade(0.5f, 0.5f));
                    seq.SetLoops(-1);
                }
            }
        }

        private void ResetDamageTiles()
        {
            if (highlightMarkers == null) return;
            
            foreach (GameObject go in highlightMarkers)
            {
                if (go != null && go.GetComponent<SpriteRenderer>() != null)
                {
                    DOTween.Kill(go);
                    Destroy(go);
                }
            }
        }

        protected void PlayerAttack()
        {
            if (!isAiming) return;
            if (thisUnit.currentWeapon == null) return;
            OnAttack?.Invoke();

            //Retrieves all targets inside the attack's Area of Damage.
            Vector2 attackOrigin = GridManager.Instance.tilemap.GetCellCenterWorld((Vector3Int) currentAimedCoord);

            List<Unit> fullDamagedUnits = new List<Unit>(), halfDamagedUnits = new List<Unit>();

            foreach (Unit u in BattleManager.Instance.UnitsInRoom)
            {
                if (Vector2.Distance(attackOrigin, u.transform.position) <= thisUnit.currentWeapon.damageArea - TileRadius)
                {
                    //Full damage
                    fullDamagedUnits.Add(u);
                }
                else if (Vector2.Distance(attackOrigin, u.transform.position) <= thisUnit.currentWeapon.damageArea - 0.01f)
                {
                    //Half damage
                    halfDamagedUnits.Add(u);
                }
            }

            List<Unit> damagedUnits = fullDamagedUnits.Concat(halfDamagedUnits).ToList();

            Weapon weaponUsed = thisUnit.currentWeapon;

            if (BattleManager.Instance.inBattle)
            {
                BattleStep damageAction = new BattleStep
                {
                    AdvanceAction = () =>
                    {
                        thisUnit.SpendAP(weaponUsed.apCost);

                        foreach (Unit full in fullDamagedUnits)
                        {
                            full.TakeDamage(weaponUsed.damage);
                        }

                        foreach (Unit half in halfDamagedUnits)
                        {
                            half.TakeDamage(weaponUsed.damage / 2);
                        }

                        weaponUsed.timesUsed++;
                        if (weaponUsed.disposable && weaponUsed.Disposed) thisUnit.GetComponent<Inventory>().BreakWeapon(weaponUsed);
                    },
                    UndoAction = () =>
                    {
                        thisUnit.RestoreAP(weaponUsed.apCost);

                        foreach (Unit full in fullDamagedUnits)
                        {
                            full.Heal(weaponUsed.damage);
                        }

                        foreach (Unit half in halfDamagedUnits)
                        {
                            half.Heal(weaponUsed.damage / 2);
                        }

                        weaponUsed.timesUsed--;
                        if (weaponUsed.disposable && !weaponUsed.Disposed) thisUnit.GetComponent<Inventory>().UnbreakWeapon(weaponUsed);
                    },
                    ActionType = ActionType.Attack
                };

                BattleStepManager.Instance.AddActionToCurrentStep(damageAction, true);
            }
            else
            {
                foreach (Unit full in fullDamagedUnits)
                {
                    full.TakeDamage(weaponUsed.damage);
                }

                foreach (Unit half in halfDamagedUnits)
                {
                    half.TakeDamage(weaponUsed.damage / 2);
                }

                weaponUsed.timesUsed++;
                if (weaponUsed.disposable && weaponUsed.Disposed) thisUnit.GetComponent<Inventory>().BreakWeapon(weaponUsed);

                if (!BattleManager.Instance.inBattle && damagedUnits.Any(du => du.faction == BattleFactions.Enemy && du.currentHp > 0))
                    BattleManager.Instance.NewBattle(thisUnit);
            }
        }

        protected void AIAttack()
        {
            if (!isAiming) return;
            OnAttack?.Invoke();
            
            //Retrieves all targets inside the attack's Area of Damage.
            Vector2 attackOrigin = GridManager.Instance.tilemap.GetCellCenterWorld((Vector3Int) currentAimedCoord);

            List<Unit> fullDamagedUnits = new List<Unit>(), halfDamagedUnits = new List<Unit>();

            foreach (Unit u in BattleManager.Instance.UnitsInRoom)
            {
                if (Vector2.Distance(attackOrigin, u.transform.position) <=
                    thisUnit.currentWeapon.damageArea - TileRadius)
                {
                    //Full damage
                    fullDamagedUnits.Add(u);
                }
                else if (Vector2.Distance(attackOrigin, u.transform.position) <=
                         thisUnit.currentWeapon.damageArea - 0.01f)
                {
                    //Half damage
                    halfDamagedUnits.Add(u);
                }
            }
            
            thisUnit.SpendAP(thisUnit.currentWeapon.apCost);
            
            foreach (Unit full in fullDamagedUnits)
            {
                full.TakeDamage(thisUnit.currentWeapon.damage);
            }
            foreach (Unit half in halfDamagedUnits)
            {
                half.TakeDamage(thisUnit.currentWeapon.damage/2);
            }
            
            if (!BattleManager.Instance.inBattle &&
                fullDamagedUnits.Concat(halfDamagedUnits).Any(du => du.faction == BattleFactions.Ally && du.currentHp > 0))
                BattleManager.Instance.NewBattle(thisUnit);
        }
    }
}