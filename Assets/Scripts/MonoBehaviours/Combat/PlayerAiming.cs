using System.Linq;
using Abstractions.Combat;
using Debugging;
using Managers.Combat;
using UnityEngine;
using UnityEngine.InputSystem;

namespace MonoBehaviours.Combat
{
    [RequireComponent(typeof(Unit))]
    public class PlayerAiming : RadialAimingSystem
    {
        private enum AimModes
        {
            Digital,
            Analog,
            Mouse
        }
        
        private Unit playerUnit;
        private InputManager inputManager;

        private AimModes aimMode = AimModes.Analog;
        private Camera cam;

        [Tooltip("Distance in tiles")]
        public float snappingDistance = 1.5f;

        private Vector2 AnalogInput => inputManager.Player.AnalogStick.ReadValue<Vector2>();
        private Vector3 MousePos => Mouse.current.position.ReadValue();

        protected override void Awake()
        {
            base.Awake();
            
            cam = Camera.main;
            playerUnit = GetComponent<Unit>();

            inputManager = new InputManager();
            inputManager.Player.Enable();
            
            inputManager.Player.Attack.performed += (context =>
            {
                if (BattleManager.Instance.inBattle && !playerUnit.CanAct) return;
                
                if(context.control.device.name == "Mouse") aimMode = AimModes.Mouse;
                IsAiming = true;
            });
            
            inputManager.Player.Attack.canceled += (context =>
            {
                if (BattleManager.Instance.inBattle)
                {
                    playerUnit.AttemptAction(PlayerAttack, playerUnit.currentWeapon.apCost);
                }
                else
                {
                    PlayerAttack();
                }
                
                IsAiming = false;
            });

            inputManager.Player.Dpad.performed += (context =>
            {
                if (BattleManager.Instance.inBattle && !playerUnit.CanAct) return;
                
                DigitalAim(context.ReadValue<Vector2>());
                aimMode = AimModes.Digital;
            });

            inputManager.Player.AnalogStick.started += (context => aimMode = AimModes.Analog);
        }
        
        private void Update()
        {
            switch (aimMode)
            {
                case AimModes.Analog:
                    if (BattleManager.Instance.inBattle)
                        SnappedAnalogPos();
                    else 
                        AnalogAim(AnalogInput);
                    break;
                case AimModes.Mouse:
                    MouseAim(BattleManager.Instance.inBattle ? SnappedMousePos() : cam.ScreenToWorldPoint(MousePos));
                    break;
                case AimModes.Digital:
                    break;
                default:
                    break;
            }
        }

        private Vector3 SnappedMousePos()
        {
            Vector2 worldMousePos = cam.ScreenToWorldPoint(MousePos);

            foreach (Unit u in BattleManager.Instance.enemiesInRoom)
            {
                if (Vector3.Distance(worldMousePos, u.transform.position) <= (snappingDistance * GridManager.Instance.tilemap.cellSize.x))
                {
                    return u.transform.position;
                }
            }

            return worldMousePos;
        }
        
        private void SnappedAnalogPos()
        {
            Vector2 worldAnalogPos = (Vector2)playerUnit.transform.position + (AnalogInput * (playerUnit.currentWeapon.range.y - (TileRadius + .1f)));
            
            if (worldAnalogPos.magnitude <= playerUnit.currentWeapon.range.x - (TileRadius/2))
                worldAnalogPos = worldAnalogPos.normalized * (playerUnit.currentWeapon.range.x - (TileRadius/2));
            
            if(DebugManager.Instance.debugMode) Debug.DrawLine(playerUnit.transform.position, worldAnalogPos, new Color(1f, 0.51f, 0f));

            Unit snappedUnit = BattleManager.Instance.enemiesInRoom.OrderBy(u => Vector3.Distance(worldAnalogPos, u.transform.position)).FirstOrDefault(u =>
                Vector3.Distance(worldAnalogPos, u.transform.position) <= (snappingDistance * GridManager.Instance.tilemap.cellSize.x));
            
            if(snappedUnit != null) TileAim(snappedUnit.CurrentCoord);
            else AnalogAim(AnalogInput);
        }

        protected override void OnDestroy()
        {
            inputManager.Player.Disable();
            
            inputManager.Player.Attack.performed -= (context =>
            {
                IsAiming = true;
            });
            inputManager.Player.Attack.canceled -= (context =>
            {
                playerUnit.AttemptAction(PlayerAttack, playerUnit.currentWeapon.apCost);
                IsAiming = false;
            });

            inputManager.Player.Dpad.performed -= (context =>
            {
                DigitalAim(context.ReadValue<Vector2>());
                aimMode = AimModes.Digital;
            });
            
            inputManager.Player.AnalogStick.started -= (context => aimMode = AimModes.Analog);

            playerUnit = null;

            base.OnDestroy();
        }
    }
}
