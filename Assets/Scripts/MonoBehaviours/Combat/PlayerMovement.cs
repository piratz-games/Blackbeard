using System.Collections.Generic;
using System.Linq;
using Abstractions;
using Abstractions.Combat;
using DG.Tweening;
using Managers.Combat;
using Sirenix.OdinInspector;
using UnityEngine;

namespace MonoBehaviours.Combat
{
    [RequireComponent(typeof(Rigidbody2D))]
    public class PlayerMovement : GridMovement
    {
        private Unit playerUnit;
        private InputManager inputManager;
        private Inventory inventory;

        private Rigidbody2D rbody;

        [BoxGroup("Analog Movement")]
        public float speed = 5;

        private bool IsHoldingBattleCommand =>
            inputManager.Player.Inventory.IsPressed() ||
            inputManager.Player.Attack.IsPressed() ||
            inputManager.Player.Scanner.IsPressed() ||
            inputManager.Player.Undo.IsPressed();

        private bool InBattle => BattleManager.Instance.inBattle;

        private bool isDirectionalNeutral = true;

        protected override void Awake()
        {
            base.Awake();

            playerUnit = ThisEntity as Unit;
            inventory = playerUnit != null ? playerUnit.GetComponent<Inventory>() : null;
            rbody = GetComponent<Rigidbody2D>();

            inputManager = new InputManager();
            inputManager.Player.Enable();
            inputManager.Player.Dpad.performed += (context =>
            {
                if (IsHoldingBattleCommand) return;
                if (BattleManager.Instance.inBattle) playerUnit.AttemptAction(() => GridMove(context.ReadValue<Vector2>()), playerUnit.moveCost);

            });

            inputManager.Player.AnalogStick.started += (context =>
            {
                if (!playerUnit.HasEnoughAp(playerUnit.moveCost)) playerUnit.OnNoActionsLeft?.Invoke();
            });

            inputManager.Player.AnalogStick.canceled += (context => isDirectionalNeutral = true);
            inputManager.Player.Dpad.canceled += (context => isDirectionalNeutral = true);

            playerUnit.GetComponent<RadialAimingSystem>().OnAttack.AddListener(() =>
            {
                if (inputManager.Player.AnalogStick.ReadValue<Vector2>().magnitude > 0 || inputManager.Player.Dpad.ReadValue<Vector2>().magnitude > 0)
                    isDirectionalNeutral = false;
            });

            inventory.OnWeaponSwitch.AddListener(() =>
            {
                if (inputManager.Player.AnalogStick.ReadValue<Vector2>().magnitude > 0 || inputManager.Player.Dpad.ReadValue<Vector2>().magnitude > 0)
                    isDirectionalNeutral = false;
            });

            inventory.OnConsumableSwitch.AddListener(() =>
            {
                if (inputManager.Player.AnalogStick.ReadValue<Vector2>().magnitude > 0 || inputManager.Player.Dpad.ReadValue<Vector2>().magnitude > 0)
                    isDirectionalNeutral = false;
            });
        }

        private void GridMove(Vector2 vec)
        {
            if (!CanMoveTo(Vector2Int.RoundToInt(vec)))
            {
                transform.DOPunchPosition(vec / 4, GridManager.Instance.moveDelay / 2, 12, 0.8f).OnComplete(() =>
                {
                    transform.DOMove(GridManager.Instance.tilemap.GetCellCenterWorld((Vector3Int) ThisEntity.CurrentCoord), 0.1f);
                });
                return;
            }

            //Get rid of diagonal movement.
            if (vec.x != 0 && vec.y != 0) vec = Vector2.zero;
            if (vec.magnitude == 0) return;

            BattleStep playerMoveAction = new BattleStep
            {
                AdvanceAction = () =>
                {
                    MoveInput(Vector2Int.RoundToInt(vec));
                    playerUnit.SpendAP(playerUnit.moveCost);
                },
                UndoAction = () =>
                {
                    UndoMove();
                    playerUnit.RestoreAP(playerUnit.moveCost);
                },
                ActionType = ActionType.Move
            };

            BattleStepManager.Instance.AddActionToCurrentStep(playerMoveAction, true);
        }

        private void FixedUpdate()
        {
            if (!isDirectionalNeutral) return;
            if (IsHoldingBattleCommand) return;

            if (BattleManager.Instance.inBattle)
            {
                if (!playerUnit.HasEnoughAp(playerUnit.moveCost)) return;
                if (OngoingLerps > 0) return;
                if (!playerUnit.CanAct) return;
            }

            Vector3 stickDir = inputManager.Player.AnalogStick.ReadValue<Vector2>();
            Vector3 dpadDir = inputManager.Player.Dpad.ReadValue<Vector2>();

            if (stickDir.magnitude <= 0 && dpadDir.magnitude <= 0) return;

            Vector3 appliedDir = stickDir.magnitude > 0 ? stickDir : dpadDir;

            Vector3 pos = transform.position;
            Vector3 nextPos = pos + appliedDir * (Time.deltaTime * speed);

            rbody.MovePosition(pos + appliedDir * (Time.deltaTime * speed));

            if (!InBattle) return;

            Vector2Int nextCoord = (Vector2Int) GridManager.Instance.tilemap.WorldToCell(nextPos);

            //Only allows movement through tiles directly in cardinals to the current tile.
            List<BattleTile> allowedTiles = GridManager.Instance.GetTileNeighbors(GridManager.Instance.GetBattleTile(ThisEntity.CurrentCoord), 1,true);
            if (ThisEntity.CurrentCoord != nextCoord && allowedTiles.All(x => x.Coordinates != nextCoord))
            {
                //Gets nearest coordinate and processes it before moving diagonally.
                Vector2Int nearestCoord = ThisEntity.CurrentCoord;
                float shortestDist = 999f;

                foreach (BattleTile t in allowedTiles)
                {
                    float dist = Vector2.Distance(transform.position, GridManager.Instance.tilemap.GetCellCenterWorld((Vector3Int) t.Coordinates));

                    if (dist < shortestDist)
                    {
                        shortestDist = dist;
                        nearestCoord = t.Coordinates;
                    }
                }

                EnteredNewTileAnalogically(nearestCoord);
                return;
            }

            if (ThisEntity.CurrentCoord != nextCoord) EnteredNewTileAnalogically(nextCoord);

        }

        private void EnteredNewTileAnalogically(Vector2Int targetCoord)
        {
            BattleStep moveAction = new BattleStep
            {
                AdvanceAction = () =>
                {
                    playerUnit.SpendAP(playerUnit.moveCost);

                    if (playerUnit.HasEnoughAp(playerUnit.moveCost))
                    {
                        AddPosToPathHistory(ThisEntity.CurrentCoord);
                        //ThisEntity.currentCoord = targetCoord;
                    }
                    else
                    {
                        LerpToCoord(targetCoord, true);
                        //ThisEntity.currentCoord = targetCoord;
                    }
                },
                UndoAction = () =>
                {
                    UndoMove();
                    playerUnit.RestoreAP(playerUnit.moveCost);
                },
                ActionType = ActionType.Move
            };

            BattleStepManager.Instance.AddActionToCurrentStep(moveAction, true);
        }
    }
}