using System.Collections.Generic;
using System.Linq;
using Abstractions.Combat;
using Managers;
using Managers.Combat;
using UnityEngine;
using UnityEngine.Events;

namespace MonoBehaviours.Combat
{
    [RequireComponent(typeof(Unit))]
    public abstract class Inventory : MonoBehaviour
    {
        protected Unit ThisUnit;

        public List<Item> items;
        protected List<Weapon> Weapons => items.Distinct().OfType<Weapon>().OrderBy(w => w.disposable).ThenBy(w => w.equipSlot).ThenBy(w => w.durability).ToList();
        protected List<Consumable> Consumables => items.Distinct().OfType<Consumable>().OrderBy(c => c.healing).ThenBy(c => c.name).ToList();
        
        protected Consumable CurrentConsumable;

        public class InventoryEvent : UnityEvent { };

        public readonly InventoryEvent OnWeaponSwitch = new InventoryEvent();
        public readonly InventoryEvent OnConsumableSwitch = new InventoryEvent();

        protected void Awake()
        {
            ThisUnit = GetComponent<Unit>();

            EquipWeapon(0);
            if (Consumables.Count > 0) CurrentConsumable = Consumables[0];
        }

        protected void WeaponSwapped(float direction)
        {
            SwapWeapon(direction < 0f);
            OnWeaponSwitch?.Invoke();
        }

        protected void PickUpLoot(DroppedLoot loot)
        {
            if (loot == null) return;

            if (!BattleManager.Instance.inBattle)
            {
                items.Add(loot.droppedItem);
                
                switch (loot.droppedItem)
                {
                    case Weapon we:
                        EquipWeapon(Weapons.IndexOf(we));
                        OnWeaponSwitch?.Invoke();
                        break;
                    case Consumable co:
                        CurrentConsumable = co;
                        OnConsumableSwitch?.Invoke();
                        break;
                }
                
                Destroy(loot.gameObject);
            }
            else
            {
                int lastEquippedWeapon = Weapons.IndexOf(ThisUnit.currentWeapon);
                Consumable lastEquippedConsumable = CurrentConsumable;

                BattleStepManager.Instance.AddActionToCurrentStep(new BattleStep
                {
                    AdvanceAction = () =>
                    {
                        items.Add(loot.droppedItem);

                        switch (loot.droppedItem)
                        {
                            case Weapon we:
                                EquipWeapon(Weapons.IndexOf(we));
                                OnWeaponSwitch?.Invoke();
                                break;
                            case Consumable co:
                                CurrentConsumable = co;
                                OnConsumableSwitch?.Invoke();
                                break;
                        }

                        loot.gameObject.SetActive(false);
                    },
                    UndoAction = () =>
                    {
                        items.Remove(loot.droppedItem);

                        switch (loot.droppedItem)
                        {
                            case Weapon we:
                                EquipWeapon(lastEquippedWeapon);
                                OnWeaponSwitch?.Invoke();
                                break;
                            case Consumable co:
                                CurrentConsumable = lastEquippedConsumable;
                                OnConsumableSwitch?.Invoke();
                                break;
                        }
                        loot.gameObject.SetActive(true);
                    },
                    ActionType = ActionType.None
                },true);
            }
        }

        private void EquipWeapon(int id)
        {
            if (Weapons[id] == null) id = 0;
            ThisUnit.currentWeapon = Weapons[id];
        }

        private void SwapWeapon(bool backwards)
        {
            if (Weapons == null || Weapons.Count <= 0) return;
            
            int currentWeaponId = Weapons.IndexOf(ThisUnit.currentWeapon);
            
            if (!backwards) EquipWeapon(currentWeaponId + 1 >= Weapons.Count ? 0 : currentWeaponId + 1);
            else EquipWeapon(currentWeaponId - 1 < 0 ? Weapons.Count - 1 : currentWeaponId - 1);

        }
        
        
        protected void ConsumableSwapped(float direction)
        {
            if (Consumables.Count <= 0) return;

            int currentConsumableId = Consumables.IndexOf(CurrentConsumable);
            int consumableCount = Consumables.Count(c => c.name == CurrentConsumable.name);

            CurrentConsumable = direction > 0f
                ? Consumables[currentConsumableId + consumableCount >= Consumables.Count ? 0 : currentConsumableId + 1]
                : Consumables[currentConsumableId - consumableCount < 0 ? Consumables.Count - 1 : currentConsumableId - 1];
            
            OnConsumableSwitch?.Invoke();
        }
        
        public void BreakWeapon(Weapon we)
        {
            items.Remove(we);
            if(Weapons.Count(w => w.name == we.name) <= 0) SwapWeapon(true);
            
            OnWeaponSwitch?.Invoke();
        }

        public void UnbreakWeapon(Weapon we)
        {   
            items.Add(we);
            EquipWeapon(Weapons.IndexOf(we));
            
            OnWeaponSwitch?.Invoke();
        }

        protected void UseConsumable(Consumable co)
        {
            if (Consumables.Count <= 0) return;
            
            if (BattleManager.Instance.inBattle)
            {
                int missingHealth = ThisUnit.maxHp - ThisUnit.currentHp;
                int amountHealed = Mathf.Min(missingHealth, co.healing);

                ThisUnit.AttemptAction(() =>
                {
                    BattleStepManager.Instance.AddActionToCurrentStep(new BattleStep
                    {
                        AdvanceAction = () =>
                        {
                            ThisUnit.SpendAP(co.apCost);

                            ConsumableUsed(co);
                            ThisUnit.Heal(co.healing);
                        },
                        UndoAction = () =>
                        {
                            items.Add(co);
                            CurrentConsumable = co;
                            OnConsumableSwitch?.Invoke();

                            ThisUnit.UndoHeal(amountHealed);

                            ThisUnit.RestoreAP(co.apCost);
                        },
                        ActionType = ActionType.Item
                    }, true);
                }, co.apCost);
            }
            else
            {
                ConsumableUsed(co);
            }
        }

        private void ConsumableUsed(Consumable co)
        {
            items.Remove(co);
            SfxManager.Instance.PlaySfx(SoundEffects.Heal, ThisUnit.GetComponent<AudioSource>());
            if (Consumables.Count(c => c.name == co.name) <= 0) ConsumableSwapped(-1f);
            Debug.Log("Consumed!");
            
            OnConsumableSwitch?.Invoke();
        }
    }
}