using Abstractions.Combat;
using DG.Tweening;
using Managers.Combat;
using Sirenix.OdinInspector;
using UnityEngine;

namespace MonoBehaviours.Combat
{
    public class DroppedLoot : BattleEntity
    {
        [AssetSelector(Paths = "Assets/ScriptableObjects/Weapons")]
        public Item droppedItem;
        
        public void Start()
        {
            Vector3 cellCenter = GridManager.Instance.tilemap.GetCellCenterWorld((Vector3Int) this.CurrentCoord);
            transform.DOMove(cellCenter, 0.5f).From(cellCenter + (Vector3.up * 1f)).SetEase(Ease.OutBounce);

            droppedItem.disposable = true;
            droppedItem.timesUsed = 0;
        }
    }
}
