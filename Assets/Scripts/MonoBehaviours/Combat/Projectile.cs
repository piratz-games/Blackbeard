using Abstractions.Combat;
using Managers.Combat;
using UnityEngine;

namespace MonoBehaviours.Combat
{
    [RequireComponent(typeof(GridMovement))]
    public class Projectile : BattleEntity
    {
        public Vector2Int speed;
        private GridMovement gridMov;

        protected override void Awake()
        {
            base.Awake();
            gridMov = GetComponent<GridMovement>();
            OnStep = Move;
        }

        private void Move()
        {
            BattleStep moveArrow = new BattleStep
            {
                AdvanceAction = () => gridMov.MoveInput(speed), 
                UndoAction = gridMov.UndoMove
            };

            BattleStepManager.Instance.AddActionToCurrentStep(moveArrow);
        }
    }
}