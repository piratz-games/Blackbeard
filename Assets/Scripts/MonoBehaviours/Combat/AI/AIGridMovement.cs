using System.Collections.Generic;
using Abstractions;
using Abstractions.Combat;
using Managers.Combat;
using UnityEngine;

namespace MonoBehaviours.Combat.AI {

    [RequireComponent(typeof(Unit))]
    public class AIGridMovement : GridMovement
    {
        private Unit thisUnit;

        protected override void Awake()
        {
            base.Awake();
            
            thisUnit = GetComponent<Unit>();

            BattleManager.Instance.OnBattleStart.AddListener(SnapToGrid);
        }

        
        public bool MoveTowardsTile(Vector2Int destination, List<BattleTile> exclusions)
        {
            BattleTile thisTile = GridManager.Instance.GetBattleTile(thisUnit.CurrentCoord);
            if (exclusions.Contains(thisTile)) exclusions.Remove(thisTile);
            
            BattleTile destinationTile = GridManager.Instance.GetBattleTile(destination);
            // if (exclusions.Contains(destinationTile)) exclusions.Remove(destinationTile);
            
            Stack<BattleTile> foundPath = AStarPathfinding.FindPath(thisTile, destinationTile, exclusions.ToArray());
            if (foundPath == null|| foundPath.Count <= 0) return false;
            
            BattleTile nextTile = foundPath.Pop();
            LerpToCoord(nextTile.Coordinates);
            
            thisUnit.SpendAP(thisUnit.moveCost);
            
            return true;
        }
    }
}