using System;
using System.Collections;
using Managers.Combat;
using UnityEngine;

namespace MonoBehaviours.Combat.AI
{
    public class AIAiming : RadialAimingSystem
    {
        public void AttackTile(Vector2Int coord, Action callback = null)
        {
            StartCoroutine(AIAimAndAttack(coord, callback));
        }

        private IEnumerator AIAimAndAttack(Vector2Int coord, Action callback = null)
        {
            IsAiming = true;
            TileAim(coord);
            yield return new WaitForSeconds(GridManager.Instance.enemyActDelay * 2f);
            AIAttack();
            IsAiming = false;
            if(!BattleManager.Instance.inBattle) yield return new WaitForSeconds(GridManager.Instance.enemyActDelay * 2f);
            callback?.Invoke();
        }
    }
}
