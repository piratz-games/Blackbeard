using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Abstractions;
using Abstractions.AI;
using Abstractions.Combat;
using Debugging;
using Helpers;
using Managers.Combat;
using UnityEngine;

namespace MonoBehaviours.Combat.AI
{
    [RequireComponent(typeof(Unit))]
    public class EnemyBattleAI : MonoBehaviour
    {
        public Tactics tactics;

        private Unit thisUnit;
        public AIGridMovement movement;
        public AIAiming aiming;

        private List<BattleTile> fledTiles;
        
        bool giveUp = false;

        private void Awake()
        {
            thisUnit = GetComponent<Unit>();
        }

        public void TurnStart()
        {
            fledTiles = new List<BattleTile>();
            EvaluateTactics();
        }

        private void EvaluateTactics()
        {
            if (tactics.priorityList == null || tactics.priorityList.Count <= 0)
            {
                BattleManager.Instance.EndTurn(tactics.defendsOnTurnEnd);
                if(DebugManager.Instance.debugMode) Debug.LogWarning($"{gameObject.name} has zero Tactics. Turn skipped.");
            }
            else
            {
                bool acted = false;
                
                for(int i = 0; i < tactics.priorityList.Count; i++)
                {
                    Tactic tactic = tactics.priorityList[i];
                    if (tactic.triggers.Count <= 0) continue;

                    if (tactic.triggers.Count == 1)
                    {
                        TacticTrigger thisTrigger = tactic.triggers[0];

                        if (TacticsEvaluations.EvaluateTrigger(thisTrigger, thisUnit))
                        {
                            acted = true;
                            giveUp = false;
                            StartCoroutine(Act(tactic.action));
                            break;
                        }
                    }
                    else
                    {
                        int triggersPassed = 0;
                    
                        foreach (TacticTrigger thisTrigger in tactic.triggers)
                        {
                            if (TacticsEvaluations.EvaluateTrigger(thisTrigger, thisUnit))
                                triggersPassed++;
                        }

                        if (triggersPassed == tactic.triggers.Count)
                        {
                            acted = true;
                            giveUp = false;
                            StartCoroutine(Act(tactic.action));
                            break;
                        }
                    }
                }
                
                if(!acted) BattleManager.Instance.EndTurn(tactics.defendsOnTurnEnd);
            }
        }

        private IEnumerator Act(TacticAction action)
        {
            switch (action)
            {
                case TacticAction.Rest:
                {
                    BattleManager.Instance.EndTurn(false);
                    if(DebugManager.Instance.debugMode) Debug.Log($"{gameObject.name} decided to REST.");
                    break;
                }
                case TacticAction.AttackLeader:
                {
                    bool isPlayerInRange =
                        Vector2.Distance(thisUnit.transform.position, BattleManager.Instance.PlayerUnit.transform.position) <=
                        thisUnit.currentWeapon.range.y;
                    
                    if(DebugManager.Instance.debugMode) Debug.Log($"{gameObject.name} decided to ATTACK the enemy LEADER.");
                    
                    if (!isPlayerInRange)
                    {
                        if(movement.MoveTowardsTile(BattleManager.Instance.PlayerUnit.CurrentCoord,GridManager.Instance.OccupiedTiles()))
                        {
                            yield return new WaitForSeconds(GridManager.Instance.enemyActDelay);
                        }
                        else
                        {
                            giveUp = true;
                            if(DebugManager.Instance.debugMode) Debug.Log($"{gameObject.name} couldn't reach its path target and decided to END TURN.");
                        }
                    }
                    else
                    {
                        aiming.AttackTile(BattleManager.Instance.PlayerUnit.CurrentCoord);
                        yield return new WaitForSeconds(GridManager.Instance.enemyActDelay * 4);
                    }
                    break;
                }
                case TacticAction.AttackNearestEnemy:
                {
                    Unit nearestEnemy = GetNearestEnemy(thisUnit.transform.position);
                    if (nearestEnemy == null) yield break;
                    
                    if(DebugManager.Instance.debugMode) Debug.Log($"{gameObject.name} decided to ATTACK the NEAREST enemy.");

                    bool nearestEnemyInRange =
                        Vector2.Distance(thisUnit.transform.position, BattleManager.Instance.PlayerUnit.transform.position) <=
                        thisUnit.currentWeapon.range.y;

                    if (!nearestEnemyInRange)
                    {
                        if(movement.MoveTowardsTile(nearestEnemy.CurrentCoord,GridManager.Instance.OccupiedTiles()))
                        {
                            yield return new WaitForSeconds(GridManager.Instance.enemyActDelay);
                        }
                        else
                        {
                            giveUp = true;
                            if(DebugManager.Instance.debugMode) Debug.Log($"{gameObject.name} couldn't reach its path target and decided to END TURN.");
                        }
                    }
                    else
                    {
                        aiming.AttackTile(nearestEnemy.CurrentCoord);
                        yield return new WaitForSeconds(GridManager.Instance.enemyActDelay * 4);
                    }
                    break;
                }
                case TacticAction.Defend:
                {
                    BattleManager.Instance.EndTurn(true);
                    if(DebugManager.Instance.debugMode) Debug.Log($"{gameObject.name} decided to DEFEND.");
                    break;
                }
                case TacticAction.Evade:
                {
                    List<BattleTile> neighbors =
                        GridManager.Instance.GetTileNeighbors(GridManager.Instance.GetBattleTile(thisUnit.CurrentCoord), 1,true);
                    List<BattleTile> availableTiles = neighbors.Except(fledTiles).ToList();
                    
                    if(DebugManager.Instance.debugMode) Debug.Log($"{gameObject.name} decided to EVADE enemies.");

                    BattleTile leastDangerousTile = GridManager.Instance.GetBattleTile(thisUnit.CurrentCoord);
                    float farthestDistance = 0f;
                    foreach (BattleTile tile in availableTiles)
                    {
                        Vector3 thisTilePos = GridManager.Instance.tilemap.GetCellCenterWorld((Vector3Int) tile.Coordinates);
                        Unit nearestEnemy = GetNearestEnemy(thisTilePos);
                        float distanceFromNearestEnemy = Vector3.Distance(thisTilePos, nearestEnemy.transform.position);

                        if (farthestDistance < distanceFromNearestEnemy)
                        {
                            leastDangerousTile = tile;
                            farthestDistance = distanceFromNearestEnemy;
                        }
                    }
                    
                    movement.MoveTowardsTile(leastDangerousTile.Coordinates, GridManager.Instance.OccupiedTiles());
                    fledTiles.Add(leastDangerousTile);
                    yield return new WaitForSeconds(GridManager.Instance.enemyActDelay);

                    break;
                }
                default:
                    BattleManager.Instance.EndTurn(tactics.defendsOnTurnEnd);
                    if(DebugManager.Instance.debugMode) Debug.Log($"{gameObject.name} didn't reach a conclusive tactic and ENDED TURN");
                    break;
            }
            
            //Takes more actions or end turn based on remaining AP
            if (thisUnit.HasEnoughAp(thisUnit.CheapestAction) && !giveUp)
            {
                EvaluateTactics();
            }
            else
            {
                BattleManager.Instance.EndTurn(tactics.defendsOnTurnEnd);
            }
        }

        private static Unit GetNearestEnemy(Vector3 pos)
        {
            Unit nearestEnemy = BattleManager.Instance.alliesInRoom
                .OrderBy(c => Vector3.Distance(pos, c.transform.position)).FirstOrDefault();
            return nearestEnemy;
        }
    }
}