using System.Linq;
using TMPro;
using UnityEngine;

namespace MonoBehaviours.Combat
{
    public class PlayerInventory : Inventory
    {
        private InputManager inputManager;
       
        public TMP_Text weaponDebug;
        public TMP_Text consumableDebug;

        private new void Awake()
        {
            base.Awake();

            inputManager = new InputManager();
            inputManager.Player.Enable();
            
            inputManager.Player.WeaponScroll.performed += ((context) => WeaponSwapped(context.ReadValue<float>()));
            inputManager.Player.Dpad.performed += ((context) =>
            {
                if (inputManager.Player.Inventory.IsPressed())
                {
                    if (Mathf.Abs(context.ReadValue<Vector2>().x) > Mathf.Abs(context.ReadValue<Vector2>().y))
                    {
                        WeaponSwapped(context.ReadValue<Vector2>().x);
                    }
                    else
                    {
                        ConsumableSwapped(context.ReadValue<Vector2>().y);
                    }
                }
            });
            inputManager.Player.DigitalStick.performed += ((context) =>
            {
                if (inputManager.Player.Inventory.IsPressed())
                {
                    if (Mathf.Abs(context.ReadValue<Vector2>().x) > Mathf.Abs(context.ReadValue<Vector2>().y))
                    {
                        WeaponSwapped(context.ReadValue<Vector2>().x);
                    }
                    else
                    {
                        ConsumableSwapped(context.ReadValue<Vector2>().y);
                    }
                }
            });
            
            inputManager.Player.UseItem.performed += ((context) => UseConsumable(CurrentConsumable));

            inputManager.Player.Action.performed += ((context =>
            {
                DroppedLoot lootInThisTile = FindObjectsOfType<DroppedLoot>()
                    .FirstOrDefault(l => l.CurrentCoord == ThisUnit.CurrentCoord);
                PickUpLoot(lootInThisTile);
            }));

            OnWeaponSwitch.AddListener(DebugShowWeaponName);
            OnConsumableSwitch.AddListener(DebugShowConsumableName);

            DebugShowWeaponName();
            DebugShowConsumableName();
        }
        
        private void DebugShowWeaponName()
        {
            if (weaponDebug != null && Weapons != null)
                weaponDebug.text = string.Format("Current weapon: [{0}] {1} (x{2})",
                    Weapons.IndexOf(ThisUnit.currentWeapon), ThisUnit.currentWeapon.name, items.Count(w => w.name == ThisUnit.currentWeapon.name));
        }
        
        private void DebugShowConsumableName()
        {
            if (consumableDebug != null && Consumables != null)
            {
                if(Consumables.Count <= 0)
                {
                    consumableDebug.text = "Consumable: None";
                }
                else
                {
                    consumableDebug.text = string.Format("Consumable: [{0}] {1} (AP:{3})(x{2})", Consumables.IndexOf(CurrentConsumable),
                        CurrentConsumable.name, items.Count(c => c.name == CurrentConsumable.name), CurrentConsumable.apCost);
                }
            }
        }
    }
}